#pragma once

#define F_CPU 8000000UL

#include <avr/io.h>
#include <util/delay.h>

/*
* Library DHT usage:
* In DHT.h
* 1. Uncomment/define F_CPU and set value
* 2. Set preferred ports(or use defaults)
* 3. In your main class: 
	readDHT(DHT type, DHTChannel dhtChannel) - set your sensor enum type(enum DHT) and sensor reading port
	recieve ReadStatus, if OK then you can get data calling get* methods
*/

#ifndef F_CPU
#error "F_CPU must be defined in DHT.h"
#endif

#define DHT_DDR DDRD
#define DHT_PORT PORTD
#define DHT_LISTENING_PIN PIND

#define DHT_CHANNEL_1_PIN PD4
#define DHT_CHANNEL_2_PIN PD5
#define DHT_CHANNEL_3_PIN PD6
#define DHT_CHANNEL_4_PIN PD7

typedef enum DHT {
	DHT11, 
	DHT22
} DHT;

typedef enum DHTChannel {
	DHT_CHANNEL_1,
	DHT_CHANNEL_2,
	DHT_CHANNEL_3,
	DHT_CHANNEL_4
} DHTChannel;

typedef enum ReadStatus {
	DHT_OK,
	DHT_ERROR_CHECKSUM,
	DHT_ERROR_TIMEOUT,
	DHT_ERROR_CONNECT,
	DHT_ERROR_ACK_L,
	DHT_ERROR_ACK_H
} ReadStatus;

#define DEGREE_SYMBOL 223		// ASCII degree symbol

ReadStatus readDHT(DHT dhtType, DHTChannel dhtChannel);

uint8_t getHumidityIntegralPart();
uint8_t getHumidityDecimalPart();
int8_t getTemperatureIntegralPart();
uint8_t getTemperatureDecimalPart();