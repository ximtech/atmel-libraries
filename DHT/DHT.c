#include "DHT.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <avr/sfr_defs.h>

#define DHT_TIMEOUT (F_CPU / 40000)

#define DHT_WAKEUP_MS 20

static uint8_t DHT_PIN;

static uint8_t integralRH;
static uint8_t decimalRH;
static int8_t integralT;	// temperature can be negative, setting signed int
static uint8_t decimalT;
static uint8_t checkSum;

void sendRequestToSensor(DHTChannel dhtChannel) {
	switch(dhtChannel) {
		case DHT_CHANNEL_1:
			DHT_PIN = DHT_CHANNEL_1_PIN;
			break;
		case DHT_CHANNEL_2:
			DHT_PIN = DHT_CHANNEL_2_PIN;
			break;
		case DHT_CHANNEL_3:
			DHT_PIN = DHT_CHANNEL_3_PIN;
			break;
		case DHT_CHANNEL_4:
			DHT_PIN = DHT_CHANNEL_4_PIN;
			break;
		default:
			DHT_PIN = 0;
	}
	
	DHT_DDR |= (1 << DHT_PIN);		// set to output
	DHT_PORT &= ~(1 << DHT_PIN);	// set to low pin
	_delay_ms(DHT_WAKEUP_MS);
	DHT_PORT |= (1 << DHT_PIN);		// set to high pin
}

ReadStatus getResponseFromSensor() {
	uint16_t timeoutCounter = DHT_TIMEOUT * 2;  // 200uSec max
	DHT_DDR &= ~(1 << DHT_PIN);		// set to input
	
	while (bit_is_set(DHT_LISTENING_PIN, DHT_PIN)) {	// wait for sensor drop pin to low level
		timeoutCounter--;
		if (timeoutCounter == 0) {
			return DHT_ERROR_CONNECT;
		}
	}
	
	timeoutCounter = DHT_TIMEOUT;
	while(bit_is_clear(DHT_LISTENING_PIN, DHT_PIN)) {  // wait for sensor set pin to high level
		timeoutCounter--;
		if (timeoutCounter == 0) {
			return DHT_ERROR_ACK_L;
		}
	}
	
	timeoutCounter = DHT_TIMEOUT;
	while(bit_is_set(DHT_LISTENING_PIN, DHT_PIN)) {	// wait for sensor start data transmission
		timeoutCounter--;
		if (timeoutCounter == 0) {
			return DHT_ERROR_ACK_H;
		}
	}
	
	return DHT_OK;
}

uint8_t setBitToDataStore(uint8_t dataStore, uint8_t dataBit) {
	return dataBit == 0x01 ? ((dataStore << 1) | 0x01) : (dataStore << 1);
}

void storeReceivedData(uint8_t bitIndex, uint8_t dataBit) {
	if (bitIndex < 8) {											// from 0 - 7 bits receiving RH integral values
		integralRH = setBitToDataStore(integralRH, dataBit);
	} else if (bitIndex >= 8 && bitIndex < 16) {				//  from 8 - 15 bits receiving RH decimal values
		decimalRH = setBitToDataStore(decimalRH, dataBit);
	} else if (bitIndex >= 16 && bitIndex < 24) {				// from 16 - 23 bits receiving temperature integral values
		integralT = setBitToDataStore(integralT, dataBit);
	} else if (bitIndex >= 24 && bitIndex < 32) {				// from 24 - 32 bits receiving temperature decimal values
		decimalT = setBitToDataStore(decimalT, dataBit);
	} else {													// final part is check sum
		checkSum = setBitToDataStore(checkSum, dataBit);
	}
}

ReadStatus readSensor(enum DHT dhtType) {
	integralRH = 0;
	decimalRH = 0;
	integralT = 0;
	decimalT = 0;
	checkSum = 0;
	
	uint16_t timeoutCounter = DHT_TIMEOUT;
	for (uint8_t i = 0; i < 40;) {		// total data size 5x8 = 40 bits
		
		if (bit_is_clear(DHT_LISTENING_PIN, DHT_PIN)) {		// wait for next bit
			
			uint16_t startDataTimeoutCounter = DHT_TIMEOUT;
			while(bit_is_clear(DHT_LISTENING_PIN, DHT_PIN)) {
				startDataTimeoutCounter--;
				if (startDataTimeoutCounter == 0) {
					return DHT_ERROR_TIMEOUT;
				}
			}
			
			_delay_us(35);	// low pulse voltage pulse is 26-28us and high pulse is 70us, so take approx high pulse middle delay
			
			if (bit_is_set(DHT_LISTENING_PIN, DHT_PIN)) {
				storeReceivedData(i, 0x01);
			} else {
				storeReceivedData(i, 0x00);
			}
			
			timeoutCounter = DHT_TIMEOUT;
			i++;
		}
		
		timeoutCounter--;
		if (timeoutCounter == 0) {
			return DHT_ERROR_TIMEOUT;
		}
		
	}
	
	return DHT_OK;
}

ReadStatus validateCheckSum() {
	uint8_t dataSum = integralRH + decimalRH + integralT + decimalT;
	return checkSum == dataSum ? DHT_OK : DHT_ERROR_CHECKSUM;
}

uint16_t combineTwo8BitValuesToOne(uint8_t integralValue, uint8_t decimalValue) {
	return (integralValue << 8) | (decimalValue & 0xFF);
}

void convertDataForDHT22() {
	uint16_t resultData = combineTwo8BitValuesToOne(integralRH, decimalRH);
	integralRH = resultData / 10;	// parse integral humidity data
	decimalRH = resultData % 10;	// parse decimal humidity data
	
	_Bool isTemperatureBelowZeroDegree = ((integralT >> 7) & 0x01) == 1;    // if temperature bits starts with one, then it is negative(see DHT22 datasheet)
	if (isTemperatureBelowZeroDegree) {
		integralT = (integralT ^ (1 << 7));      // replace first bit to zero, because it is temperature sign and no need to use it for temp calculation
		resultData = combineTwo8BitValuesToOne(integralT, decimalT);
		integralT = -(resultData / 10);	// parse integral temperature data and set it negative
		decimalT = resultData % 10;		// parse decimal temperature data
		
	} else {
		resultData = combineTwo8BitValuesToOne(integralT, decimalT);
		integralT = resultData / 10;
		decimalT = resultData % 10;
	}
	
}

ReadStatus readDHT(DHT dhtType, DHTChannel dhtChannel) {
	sendRequestToSensor(dhtChannel);
	enum ReadStatus status = getResponseFromSensor();
	
	if (status != DHT_OK) {
		return status;
	}
	
	status = readSensor(dhtType);
	
	if (status == DHT_OK) {
		status = validateCheckSum();
	}
	
	if (dhtType == DHT22) {
		convertDataForDHT22();
	}
		
	return status;
}

uint8_t getHumidityIntegralPart() {
	return integralRH;
}

uint8_t getHumidityDecimalPart() {
	return decimalRH;
}

int8_t getTemperatureIntegralPart() {
	return integralT;
}

uint8_t getTemperatureDecimalPart() {
	return decimalT;
}

