#include "SevenSegment.h"
#include <avr/pgmspace.h>
#include <stdarg.h>

const unsigned char asciiTable[] PROGMEM = {
  0x00, 0x86, 0x22, 0x7E, 0x7B, 0x63, 0x7E, 0x02,  /* !"#$%&'*/
  0x39, 0x0F, 0x63, 0x70, 0x80, 0x40, 0x80, 0x52,  /*()*+,-./*/
  0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07,  /*01234567*/
  0x7F, 0x6F, 0x00, 0x00, 0x58, 0x48, 0x4C, 0xA7,  /*89:;<=>?*/
  0x5C, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71, 0x3D,  /*@ABCDEFG*/
  0x76, 0x06, 0x1E, 0x72, 0x38, 0x55, 0x54, 0x3F,  /*HIJKLMNO*/
  0x73, 0x67, 0x50, 0x6D, 0x78, 0x3E, 0x1C, 0x1D,  /*PQRSTUVW*/
  0x49, 0x6E, 0x5b, 0x39, 0x64, 0x0F, 0x23, 0x08   /*XYZ[\]^_*/
};

static SegmentType segmentType = COMMON_CATHODE;
static DigitCount digitCount = ONE_DIGIT;

static uint8_t segmentPinArray[SEGMENT_COUNT] = { [0 ... (SEGMENT_COUNT - 1)] = 0 };
static uint8_t digtPinArray[FOUR_DIGITS] = { [0 ... (FOUR_DIGITS - 1)] = 0 };
	
static void writeToPort(uint8_t portPin, uint8_t value);
static void writeValueToSegments(uint8_t value);
	
void initSevenSegment(SegmentType segType, DigitCount digCount, ...) {
	segmentType = segType;
	digitCount = digCount;
	SEGMENT_DDR = 0xFF;	// all segment port has only outputs
	SEGMENT_PORT = 0;	// disable all segment outputs
	
	segmentPinArray[0] = SEGMENT_A;
	segmentPinArray[1] = SEGMENT_B;
	segmentPinArray[2] = SEGMENT_C;
	segmentPinArray[3] = SEGMENT_D;
	segmentPinArray[4] = SEGMENT_E;
	segmentPinArray[5] = SEGMENT_F;
	segmentPinArray[6] = SEGMENT_G;
	segmentPinArray[7] = SEGMENT_DP;
	
	va_list valist;
	va_start(valist, digCount);	// initialize valist for digit PIN number of arguments
	
	for (uint8_t i = 0; i < digitCount; i++) {
		digtPinArray[i] = va_arg(valist, unsigned int);
		DIGIT_DDR |= (1 << digtPinArray[i]);		// set to output
		
		if (segmentType == COMMON_CATHODE) {
			DIGIT_PORT |= (1 << digtPinArray[i]);
		} else {
			DIGIT_PORT &= ~(1 << digtPinArray[i]);
		}
	}
	
	va_end(valist);	// clean memory reserved for valist
}

void selectDigit(uint8_t digit) {
	if (digit <= digitCount) {
		for (uint8_t i = 0; i < digitCount; i++) {		// first turn off all digits
			if (segmentType == COMMON_CATHODE) {
				DIGIT_PORT |= (1 << digtPinArray[i]);
			} else {
				DIGIT_PORT &= ~(1 << digtPinArray[i]);
			}
		}
		
		if (segmentType == COMMON_CATHODE) {	// then turn on digit
			DIGIT_PORT &= ~(1 << digtPinArray[digit - 1]);
		} else {
			DIGIT_PORT |= (1 << digtPinArray[digit - 1]);
		}
		
	}
}

void displayNumber(uint8_t numberToDisplay) {
	if (numberToDisplay >= 0 && numberToDisplay <= 9) {
		displayChar(numberToDisplay + '0');
	}
}

void displayChar(unsigned char symbol) {
	unsigned char value = pgm_read_byte(&(asciiTable[symbol - ' ']));
	writeValueToSegments(value);
}

void powerOnDigits() {
	for (uint8_t i = 0; i < digitCount; i++) {		// turn on all digits
		if (segmentType == COMMON_CATHODE) {
			DIGIT_PORT &= ~(1 << digtPinArray[i]);
		} else {
			DIGIT_PORT |= (1 << digtPinArray[i]);
		}
	}
}

void powerOffDigits() {
	for (uint8_t i = 0; i < digitCount; i++) {		// turn off all digits
		if (segmentType == COMMON_CATHODE) {
			DIGIT_PORT |= (1 << digtPinArray[i]);
		} else {
			DIGIT_PORT &= ~(1 << digtPinArray[i]);
		}
	}
}

static void writeValueToSegments(uint8_t value) {
	for (uint8_t i = 0; i < SEGMENT_COUNT; i++) {
		if (segmentType == COMMON_CATHODE) {
			writeToPort(segmentPinArray[i], (value >> i) & 0x01);
		} else {
			writeToPort(segmentPinArray[i], ~(value >> i) & 0x01);
		}
	}
}

static void writeToPort(uint8_t portPin, uint8_t value) {
	if (value) {
		SEGMENT_PORT |= (1 << portPin);
	} else {
		SEGMENT_PORT &= ~(1 << portPin);
	}
}
