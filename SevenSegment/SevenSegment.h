#pragma once

#include <avr/io.h>
#include <inttypes.h>

#define SEGMENT_COUNT 8

#define SEGMENT_DDR  DDRB
#define SEGMENT_PORT PORTB
#define SEGMENT_A  DDB0
#define SEGMENT_B  DDB1
#define SEGMENT_C  DDB2
#define SEGMENT_D  DDB3
#define SEGMENT_E  DDB4
#define SEGMENT_F  DDB5
#define SEGMENT_G  DDB6
#define SEGMENT_DP DDB7

#define DIGIT_DDR  DDRD
#define DIGIT_PORT PORTD

typedef enum SegmentType {
	COMMON_CATHODE, COMMON_ANODE
} SegmentType;

typedef enum DigitCount {
	ONE_DIGIT =    1,
	TWO_DIGITS =   2,
	THREE_DIGITS = 3,
	FOUR_DIGITS =  4
} DigitCount;

void initSevenSegment(SegmentType segType, DigitCount digCount, ...);	// digit pins: Example DDD0, DDD1 for two digits

void selectDigit(uint8_t digit);
void displayNumber(uint8_t numberToDisplay);
void displayChar(unsigned char symbol);

void powerOnDigits();
void powerOffDigits();