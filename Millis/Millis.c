#include "Millis.h"

#include <util/atomic.h>

volatile uint64_t timer1Millis; //NOTE: A uint64_t holds values from 0 to 18.446.744.073.709.551.615 (2^64 - 1). Its 584.942417 years ~585 million years.

ISR(TIMER1_COMPA_vect) {
  timer1Millis++;  
}

void initMillis() {
  uint32_t ctcMatchOverflow = ((F_CPU / 1000) / 8);		//when timer1 is this value, 1ms has passed
  resetMillis();
  
 #ifdef ATMEGA8
  TCCR1B |= (1 << WGM12) | (1 << CS11);		// (Set timer to clear when matching ctcMatchOverflow) | (Set clock divisor to 8)
  OCR1A = ctcMatchOverflow;					// Set timer compare value
  TIMSK |= (1 << OCIE1A);					// Enable the compare match interrupt
  TCNT1 = 0;								// initialize counter
 #endif
  
 #ifdef ATMEGA328
   TCCR1B |= (1 << WGM12) | (1 << CS11);
   OCR1A = ctcMatchOverflow;
   TIMSK1 |= (1 << OCIE1A);
   TCNT1 = 0;
 #endif
  
  sei();
}

uint64_t millis() {
  uint64_t millisToReturn;

  ATOMIC_BLOCK(ATOMIC_FORCEON) {	// Ensure this cannot be disrupted
    millisToReturn = timer1Millis;
  }
  return millisToReturn;
}

void resetMillis() {
	timer1Millis = 0;
}