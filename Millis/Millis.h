#pragma once
/* 
* The millis() function known from Arduino
* Calling millis() will return the milliseconds since the program started
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

//#define F_CPU 8000000UL

#define ATMEGA8
//#define ATMEGA328

#ifndef F_CPU
#error "F_CPU not defined in Millis.h"
#endif

void initMillis();
uint64_t millis();
void resetMillis();