#include "RTC_I2C.h"

#include <stdint.h>
#include <stdbool.h>
#include <avr/sfr_defs.h>

static char * const WEEK_DAY_NAME_SHORT[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
static _Bool isRtcConfigured = false;

// set initial values
static LocalTime localTime = { 0, 0, 0, HOUR_FORMAT_24, AM};
static LocalDate localDate = { 1, JANUARY, 2000, SUNDAY };
static LocalDateTime localDateTime = { 0, 0, 0, 1, JANUARY, 2000, SUNDAY, HOUR_FORMAT_24, AM };
	
static inline uint8_t parseReceivedData(uint8_t dataBits);	
static inline uint8_t parseOutputData(uint8_t dataBits);
static inline uint8_t parseYear(uint16_t year);
static inline HourFormat getHourFormat(uint8_t hours);
static inline AmPm getHourAmPm(uint8_t hours);
static inline uint8_t removeUnusedBitsInReceivedHours(uint8_t hours);
static inline uint8_t set24HourFormat(uint8_t hours);
static inline uint8_t set12HourFormat(uint8_t hours, AmPm amPm);


void initRTC() {
	initMasterI2C(I2C_PRESCALER_1);	// initialize I2C
	isRtcConfigured = true;
}

void setRTCtime(uint8_t hours, uint8_t minutes, uint8_t seconds) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);			// start I2C communication with RTC
		writeMasterI2C(RTC_TIME_START_ADDRESS);		// write 0 address for second
		writeMasterI2C(parseOutputData(seconds));	// write second on 00 location
		writeMasterI2C(parseOutputData(minutes));	// write minute on 01(auto increment) location
		writeMasterI2C(set24HourFormat(hours));		// write hour on 02 location
		stopMasterI2C();							// stop I2C communication
	}
}

void setRTCtime12hFormat(uint8_t hours, uint8_t minutes, uint8_t seconds, AmPm amPm) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);				// start I2C communication with RTC
		writeMasterI2C(RTC_TIME_START_ADDRESS);			// write 0 address for second
		writeMasterI2C(parseOutputData(seconds));		// write second on 00 location
		writeMasterI2C(parseOutputData(minutes));		// write minute on 01(auto increment) location
		writeMasterI2C(set12HourFormat(hours, amPm));	// write hour on 02 location
		stopMasterI2C();								// stop I2C communication
	}
}

void setRTCdate(uint8_t day, Month month, uint16_t year) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);					// start I2C communication with RTC
		writeMasterI2C(RTC_DATE_START_ADDRESS);				// write 3 address for day
		writeMasterI2C(getDayOfWeek(day, month, year));		// write week day on 03 location
		writeMasterI2C(parseOutputData(day));				// write date on 04 location
		writeMasterI2C(parseOutputData(month));				// write month on 05 location
		writeMasterI2C(parseYear(year));					// write year on 06 location
		stopMasterI2C();									// stop I2C communication
	}
}

LocalTime * getRTCtime() {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);								// start I2C communication with RTC
		writeMasterI2C(RTC_TIME_START_ADDRESS);							// write 0 address for second
		startMasterI2C(RTC_READ_ADDRESS);								// start I2C communication with RTC
		localTime.second = parseReceivedData(readMasterI2C());			// read seconds
		localTime.minute = parseReceivedData(readMasterI2C());			// read minutes
		
		uint8_t hours = readMasterNackI2C();							// read hour with Nack
		localTime.hourFormat = getHourFormat(hours);
		localTime.amPm = getHourAmPm(hours);
		hours = removeUnusedBitsInReceivedHours(hours);
		localTime.hour = parseReceivedData(hours);						// convert received hours to decimal value
		stopMasterI2C();												// stop i2C communication
	}
	
	return &localTime;
}

LocalDate * getRTCdate() {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);									// start I2C communication with RTC
		writeMasterI2C(RTC_DATE_START_ADDRESS);								// write 0 address for second
		startMasterI2C(RTC_READ_ADDRESS);									// start I2C communication with RTC
		localDate.weekDay = readMasterI2C();								// read week day
		localDate.day = parseReceivedData(readMasterI2C());					// read date
		localDate.month = parseReceivedData(readMasterI2C());				// read month
		localDate.year = MIN_YEAR + parseReceivedData(readMasterNackI2C());	// read the year with Nack
		stopMasterI2C();													// stop i2C communication
	}
	
	return &localDate;
}

LocalDateTime * getRTCdateTime() {
	LocalTime *localTime = getRTCtime();
	LocalDate *localDate = getRTCdate();
	
	localDateTime.hour = localTime->hour;
	localDateTime.minute = localTime->minute;
	localDateTime.second = localTime->second;
	localDateTime.amPm = localTime->amPm;
	localDateTime.hourFormat = localTime->hourFormat;
	localDateTime.day = localDate->day;
	localDateTime.month = localDate->month;
	localDateTime.year = localDate->year;
	localDateTime.weekDay = localDate->weekDay;
	
	return &localDateTime;
}

void setRTChoursIn24hFormat(uint8_t hours) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_HOURS_ADDRESS);
		writeMasterI2C(set24HourFormat(hours));		// write hour on 02 location
		stopMasterI2C();							// stop I2C communication
	}
}

void setRTChoursIn12hFormat(uint8_t hours, AmPm amPm) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_HOURS_ADDRESS);
		writeMasterI2C(set12HourFormat(hours, amPm));		// write hour on 02 location
		stopMasterI2C();									// stop I2C communication
	}
}

void setRTCminutes(uint8_t minutes) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_MINUTES_ADDRESS);
		writeMasterI2C(parseOutputData(minutes));				// write minute on 01(auto increment) location
		stopMasterI2C();									// stop I2C communication
	}
}

void setRTCday(uint8_t day) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_DAY_ADDRESS);
		writeMasterI2C(parseOutputData(day));						// write date on 04 location
		stopMasterI2C();									// stop I2C communication
	}
}

void setRTCmonth(uint8_t month) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_MONTH_ADDRESS);
		writeMasterI2C(parseOutputData(month));					// write month on 05 location
		stopMasterI2C();									// stop I2C communication
	}
}

void setRTCyear(uint16_t year) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_YEAR_ADDRESS);
		writeMasterI2C(parseYear(year));					// write year on 06 location
		stopMasterI2C();									// stop I2C communication
	}
}

void setRTCweekDay(WeekDay weekDay) {
	if (isRtcConfigured) {
		startMasterI2C(RTC_WRITE_ADDRESS);
		writeMasterI2C(RTC_WEEK_DAY_ADDRESS);
		writeMasterI2C(weekDay);							// write week day on 03 location
		stopMasterI2C();									// stop I2C communication
	}
}

void setWeekDayByDate(uint8_t day, uint8_t month, uint16_t year) {
	setRTCweekDay(getDayOfWeek(day, month, year));
}

char * getWeekDayNameShort(WeekDay weekDay) {
	return WEEK_DAY_NAME_SHORT[weekDay - 1];
}

static inline uint8_t parseReceivedData(uint8_t dataBits) {
	return (((dataBits & 0xF0) >> 4) * 10) + (dataBits & 0x0F);		// parse incoming bits for correct decimal representation. Remove highest bit with mask(0b01110000) to prevent incorrect data store
}

static inline uint8_t parseOutputData(uint8_t dataBits) {
	return ((dataBits / 10) << 4) | (dataBits % 10);	// parse hour and place bits, Example: 23 -> parsed to 2 and 3, 2 is placed at Bit5 and Bit4, lower part is placed from 0-3 bits
}

static inline uint8_t parseYear(uint16_t year) {
	return parseOutputData(year % 100);				// get only last two year digits
}

static inline HourFormat getHourFormat(uint8_t hours) {
	return bit_is_set(hours, RTC_HOUR_FORMAT_BIT) ? HOUR_FORMAT_12 : HOUR_FORMAT_24;
}

static inline AmPm getHourAmPm(uint8_t hours) {
	return bit_is_set(hours, RTC_AM_PM_BIT) ? PM : AM;
}

static inline uint8_t removeUnusedBitsInReceivedHours(uint8_t hours) {
	if (getHourFormat(hours) == HOUR_FORMAT_12) {
		hours &= ~(1 << RTC_AM_PM_BIT);				// clear AM/PM bit if hour format is 12h for 24h this bit represents 2
	}
	hours &= ~(1 << RTC_HOUR_FORMAT_BIT);		// clear hour format bit
	return hours;
}

static inline uint8_t set24HourFormat(uint8_t hours) {
	uint8_t resultValue = 0;
	hours = hours & 0x1F;						// clean unused bits for 24h format
	resultValue &= ~(1 << RTC_HOUR_FORMAT_BIT);
	resultValue = parseOutputData(hours);
	return resultValue;
}

static inline uint8_t set12HourFormat(uint8_t hours, AmPm amPm) {
	uint8_t resultValue = 0;
	hours = hours & 0x0F;						// clean unused bits for 12h format
	resultValue = parseOutputData(hours);
	resultValue |= (1 << RTC_HOUR_FORMAT_BIT);
	
	if (amPm == PM) {
		resultValue |= (1 << RTC_AM_PM_BIT);
	} else {
		resultValue &= ~(1 << RTC_AM_PM_BIT);
	}
	return resultValue;
}
