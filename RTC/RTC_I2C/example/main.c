#include <avr/io.h>
#include "LCD16x2.h"
#include "RTC_I2C.h"


int main(void) {
    
	initLCD();
	printLCDString("Test");
	
	initRTC();
	setRTCtime(22, 12, 43);
	setRTCdate(15, DECEMBER, 1989);
	
	LocalDateTime *localDateTime = getRTCdateTime();
	
    while (1) {
		
		localDateTime = getRTCdateTime();
	
		goToXYLCD(0, 0);
		printfLCD("%02d:%02d:%02d  ", localDateTime->hour, localDateTime->minute, localDateTime->second);
		
		
		goToXYLCD(1, 0);
		printfLCD("%02d/%02d/%02d %3s ", localDateTime->day, localDateTime->month, localDateTime->year, getWeekDayNameShort(localDateTime->weekDay));
    }
	
	
}

