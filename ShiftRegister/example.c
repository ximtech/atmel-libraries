#define F_CPU 8000000UL

#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>

#include "74HC595.h"

#define DELAY 50


int main() {
	
	initShiftRegister();
	
	uint16_t binary_counter = 0;
	
    while (true) {
		
		sendByteToShiftRegister(binary_counter);
		sendByteToShiftRegister(binary_counter >> 8);
		
		latchShiftRegister();
		
		binary_counter++;
		
		_delay_ms(DELAY);
		
    }
	
}

