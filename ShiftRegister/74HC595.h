#pragma once

#include <avr/io.h>
#include <stdint.h>

#define USE_HARDWARE_SPI

#define HC595_COUNT 1

// define SPI bus pins
#define SHIFT_DDR DDRB
#define SHIFT_PORT PORTB

#define MR PB0	// reset register pin

#ifdef USE_HARDWARE_SPI
#define MOSI PB3	// DS
#define SCK  PB5	// SH_CP
#define SS   PB2	// ST_CP

#else
#define SH_CP PB5	// clocking
#define ST_CP PB2	// data latch
#define DS    PB3	// input data

#endif // USE_HARDWARE_SPI

void initShiftRegister();
void sendByteToShiftRegister(uint8_t byte);
void sendByteArrayToShiftRegister(uint8_t byteArray[HC595_COUNT]);
void latchShiftRegister();
void resetShiftRegister();