#include "74HC595.h"

void initShiftRegister() {
	SHIFT_DDR |= (1 << MR);
	resetShiftRegister();	
	
#ifdef USE_HARDWARE_SPI
	SHIFT_DDR |= (1 << MOSI) | (1 << SCK) | (1 << SS);		// set MOSI, SS and SCK as outputs
	SHIFT_PORT &= ~((1 << MOSI) | (1 << SCK) | (1 << SS));	// set MOSI, SS and SCK to low
	SHIFT_PORT &= ~(1 << SS);								// pull SS low (Important: this is necessary to start the SPI transfer!)
	SPSR &= ~(1 << SPI2X);									// disable speed doubler
	SPCR |= (1 << SPE) | (1 << MSTR);						// enable SPI in master mode
#else
	SHIFT_DDR |= (1 << SH_CP) | (1 << ST_CP) | (1 << DS);
	SHIFT_PORT &= ~((1 << SH_CP) | (1 << ST_CP) | (1 << DS));
#endif // USE_HARDWARE_SPI
	
};

void sendByteToShiftRegister(uint8_t byte) {
#ifdef USE_HARDWARE_SPI
	SPDR = byte;                         // set the data byte and start the transfer
	while (!(SPSR & (1 << SPIF)));       // wait for the transfer to complete
#else
	for (uint8_t i = 8; i > 0; i--) {
		uint8_t bit = (byte >> (i - 1)) & 0x01;	// reverse data order
		if (bit) {								// set data bit to shift register input
			SHIFT_PORT |= (1 << DS);	
		} else {
			SHIFT_PORT &= ~(1 << DS);
		}
			
		SHIFT_PORT &= ~(1 << SH_CP);	// then impulse to clock input
		SHIFT_PORT |= (1 << SH_CP);
	}
#endif // USE_HARDWARE_SPI
}

void sendByteArrayToShiftRegister(uint8_t byteArray[HC595_COUNT]) {
	for(uint8_t i = HC595_COUNT; i > 0; i--) {
		sendByteToShiftRegister(byteArray[i - 1]);
	}
	latchShiftRegister();
}

void latchShiftRegister() {
#ifdef USE_HARDWARE_SPI
	SHIFT_PORT |= (1 << SS);
	SHIFT_PORT &= ~(1 << SS);
#else
	SHIFT_PORT |= (1 << ST_CP);
	SHIFT_PORT &= ~(1 << ST_CP);
#endif // USE_HARDWARE_SPI
}

void resetShiftRegister() {
	SHIFT_PORT &= ~(1 << MR);
	SHIFT_PORT |= (1 << MR);	// set to hight(shift register is ready to receive data)
}