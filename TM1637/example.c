#include <avr/io.h>

#include "libs/TM1637.h"
#include <util/delay.h>


int main() {
	initTM1637();
	setNumber(1637);

	for (uint8_t b = 0; b != 8; ++b) {
		setBrightness(b);
		_delay_ms(500);
	}

	clear();

	uint16_t i = 0;
	while (i <= 9999)
	setNumber(i++);

	scrollChars("Hello World");

	setChars("done");

	while (1) {
	}
	
	
}

