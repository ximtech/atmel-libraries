#pragma once

//#define F_CPU 8000000UL

#include <stdint.h>
#include <avr/sfr_defs.h>

// ----------------------------------------------------------------------------
// PIN / PORT configuration
// NOTE: All three pins used must be bits on the same PORT register (ex. PORTD)
// ----------------------------------------------------------------------------

#define TM_OUT PORTD
#define TM_IN  PIND
#define TM_DDR DDRD
#define TM_BIT_CLK _BV(PD0)
#define TM_BIT_DAT _BV(PD1)

// ----------------------------------------------------------------------------
// Functions and parameters
// ----------------------------------------------------------------------------

// Parameters for setNumber()
#define TM_RIGHT 0x01
#define TM_LEFT 0x00

// Parameters for setNumberPad()
#define TM_PAD_SPACE 0x00
#define TM_PAD_0 0x3F

#define TM1637_DIGITS 4

// Control the TM1637 chip. This chip is usually attached
// to inexpensive 4 digit numeric displays. It's a generic
// 7-segment driver with key input capabilities though so
// it can be used for other I/O boards.
void initTM1637();

// Clear the 7-segment displays (only)
void clear();

// Set a single 7-segment display to the given byte value.
// This allows direct control of the elements to do spinning animations etc.
void setByte(const uint8_t position, const uint8_t b);

// Display a single digit at the given position.
// Position is left-to-right, starting with 0.
void setDigit(const uint8_t position, const uint8_t digit);

// Display an unsigned number.
void setNumber(uint32_t number);

// Display an unsigned number at a given offset and pad it with 0's or
// spaces to a desired with. This function is helpful when the numbers can
// fluctuate in length (ex. 100, 5, 1000) to avoid flickering segments.
void setNumberPad(uint32_t number, uint8_t offset, uint8_t width);

// Display an unsigned number in hex format at a given offset it
// with 0's or spaces to a desired with.
void setNumberHex(uint32_t number, uint8_t offset, uint8_t width);

// Draw a character at a given position.
// Not all characters are supported, check TM1637Font.h for an overview.
void setChar(const uint8_t position, const char ch);

// Display a string.
void setChars(const char *value);

// Scroll characters (scrolls <--- left)
void scrollChars(const char *value);

// Set which "dots" should be enabled.
// Mask is mapped right to left (ex. 0x01 = right-most dot)
void setDots(const uint8_t mask);

// Set the brightness between 0 and 7
void setBrightness(const uint8_t brightness);