#define F_CPU 8000000UL			/* Define CPU Frequency e.g. here 8MHz */
#include <avr/io.h>			/* Include AVR std. library file */
#include <util/delay.h>			/* Include inbuilt defined Delay header file */

#include "lib/LCD16x2.h"

// rolling display delay, change this define for controlling rolling speed
#define ROLL_RIGHT_DELAY_MS 200
#define ROLL_LEFT_DELAY_MS 200

void rollDisplayRight(uint8_t shiftCount) {
	for (uint8_t i = 0; i < shiftCount; i++) {
		moveDisplayRightLCD();
		_delay_ms(ROLL_RIGHT_DELAY_MS);
	}
}

void rollDisplayLeft(uint8_t shiftCount) {
	for (uint8_t i = 0; i < shiftCount; i++) {
		moveDisplayLeftLCD();
		_delay_ms(ROLL_LEFT_DELAY_MS);
	}
}

void animationLCDString() {
	printLCDString("Hello World!");
	uint8_t shift = 16;
	while(1) {
		rollDisplayRight(shift);
		shift = 29;
		rollDisplayLeft(shift);
	}
}

void createAndPrintCustomCharacters() {
	uint8_t Character1[8] = { 0x00, 0x0A, 0x15, 0x11, 0x0A, 0x04, 0x00, 0x00 };  /* Custom char set for alphanumeric LCD Module */
	uint8_t Character2[8] = { 0x04, 0x1F, 0x11, 0x11, 0x1F, 0x1F, 0x1F, 0x1F };
	uint8_t Character3[8] = { 0x04, 0x0E, 0x0E, 0x0E, 0x1F, 0x00, 0x04, 0x00 };
	uint8_t Character4[8] = { 0x01, 0x03, 0x07, 0x1F, 0x1F, 0x07, 0x03, 0x01 };
	uint8_t Character5[8] = { 0x01, 0x03, 0x05, 0x09, 0x09, 0x0B, 0x1B, 0x18 };
	uint8_t Character6[8] = { 0x0A, 0x0A, 0x1F, 0x11, 0x11, 0x0E, 0x04, 0x04 };
	uint8_t Character7[8] = { 0x00, 0x00, 0x0A, 0x00, 0x04, 0x11, 0x0E, 0x00 };
	uint8_t Character8[8] = { 0x00, 0x0A, 0x1F, 0x1F, 0x0E, 0x04, 0x00, 0x00 };
		
		createCustomCharacter(0, Character1);
		createCustomCharacter(1, Character2);
		createCustomCharacter(2, Character3);
		createCustomCharacter(3, Character4);
		createCustomCharacter(4, Character5);
		createCustomCharacter(5, Character6);
		createCustomCharacter(6, Character7);
		createCustomCharacter(7, Character8);
		
		printLCDString("Custom char LCD!");
		moveCursorAtSecondLineBegginingLCD();
		
		for(char i = 0; i < 8; i++) {
			printLCDCustomCharacter(i);
			printLCDChar(' ');
		}
}

int main() {

	initLCD();
	
	printfLCD("Format test: %c", 'A');
	moveCursorAtSecondLineBegginingLCD();
	printfLCD("Second test ");
	
		//_delay_ms(2000);
		//
		//clearLCD();
		//animationLCDString();

	return 0;
}

