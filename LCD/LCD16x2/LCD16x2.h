#pragma once

//#define F_CPU 8000000UL

#include <avr/io.h>
#include <stdio.h>
/*
* Library LCD16x2 usage:
* In LCD16x2.h
* 1. Uncomment F_CPU and set value
* 2. Uncomment LCD working mode(USE_8_BIT_MODE or USE_4_BIT_MODE, but not both). By default configured for 8 bit mode.
* 3. Set preferred ports(or use defaults) in LCD mode block
* 4. In your main class: 
	initLCD(); // by default 2 lines is initialized, cursor is not showing and writing from left to right
	then you start use LCD
*/


#define USE_8_BIT_MODE		// define LCD working mode
//#define USE_4_BIT_MODE


#ifdef USE_8_BIT_MODE	// if not redefined, then default ports will be used for 8 bit mode
#define LCD_DATA_DDR DDRB
#define LCD_COMMAND_DDR DDRC

#define LCD_DATA_PORT PORTB
#define LCD_COMMAND_PORT PORTC

#define LCD_RS DDC2
#define LCD_RW DDC3
#define LCD_EN DDC4

#define LCD_D0 DDB0
#define LCD_D1 DDB1
#define LCD_D2 DDB2
#define LCD_D3 DDB3
#define LCD_D4 DDB4
#define LCD_D5 DDB5
#define LCD_D6 DDB6
#define LCD_D7 DDB7
#endif	//USE_8_BIT_MODE


#ifdef USE_4_BIT_MODE	// if not redefined, then default ports will be used for 4 bit mode
#define LCD_DDR  DDRB
#define LCD_PORT PORTB

#define LCD_RS DDB0
#define LCD_EN DDB1
#define LCD_RW DDB2		// optional, if not connected, wire this LCD pin to ground

#define LCD_D4 DDB4
#define LCD_D5 DDB5
#define LCD_D6 DDB6
#define LCD_D7 DDB7
#endif	//USE_4_BIT_MODE

#ifndef F_CPU
#error "F_CPU must be defined in LCD16x2.h"
#endif

#if !defined(USE_8_BIT_MODE) && !defined(USE_4_BIT_MODE)
#error "LCD mode is not defined. Should be four(USE_4_BIT_MODE) or eight(USE_8_BIT_MODE) bit mode defined"
#endif

#if defined(USE_8_BIT_MODE) && defined(USE_4_BIT_MODE)
#error "LCD mode settings is incorrect. Should be set only four(USE_4_BIT_MODE) or eight(USE_8_BIT_MODE) bit mode only"
#endif

#define LCD_COL_COUNT 16
#define LCD_ROW_COUNT 2

// Basic commands
#define LCD_CLEAR_DISPLAY 0x01			// Clear the display screen. Execution Time: 1.64ms
#define LCD_SHIFT_CURSOR_RIGHT 0x06		// Shift the cursor right (e.g. data gets written in an incrementing order, left to right)

#define LCD_DISPLAY_ON_CURSOR_OFF 0x0C
#define LCD_DISPLAY_ON_CURSOR_BLINKING 0x0E

#define LCD_MOVE_CURSOR_AT_FIRST_LINE_BEGINNING 0x80	// Force the cursor to the beginning of the 1st line
#define LCD_MOVE_CURSOR_AT_SECOND_LINE_BEGINNING 0xC0	// Force the cursor to the beginning of the 2nd line

#define LCD_SHIFT_POSITION_LEFT 0x10					// Shift cursor position to the left
#define LCD_SHIFT_POSITION_RIGHT 0x14					// Shift cursor position to the right
#define LCD_SHIFT_DISPLAY_LEFT 0x18						// Shift entire display to the left
#define LCD_SHIFT_DISPLAY_RIGHT 0x1C					// Shift entire display to the right

#define LCD_TWO_LINES_5X8_MATRIX_8BIT_MODE 0x38
#define LCD_TWO_LINES_5X8_MATRIX_4BIT_MODE 0x28
#define LCD_ONE_LINE_8BIT_MODE 0x30
#define LCD_ONE_LINE_4BIT_MODE 0x20

// commands
#define LCD_CLEARDISPLAY   0x01
#define LCD_RETURNHOME     0x02
#define LCD_ENTRYMODESET   0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT    0x10
#define LCD_FUNCTIONSET    0x20
#define LCD_SETCGRAMADDR   0x40
#define LCD_SETDDRAMADDR   0x80

#define LCD_ENTRYRIGHT          0x00
#define LCD_ENTRYLEFT           0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON  0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON   0x02
#define LCD_CURSOROFF  0x00
#define LCD_BLINKON    0x01
#define LCD_BLINKOFF   0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE  0x00
#define LCD_MOVERIGHT   0x04
#define LCD_MOVELEFT    0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE    0x08
#define LCD_1LINE    0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS  0x00


void initLCD();
void commandLCD(uint8_t command);

void clearLCD();

void turnOnLCD();
void turnOffLCD();

void returnHomeLCD();
void moveCursorAtSecondLineBeginningLCD();

void enableBlinkingCursorLCD();
void disableBlinkingCursorLCD();
void enableCursorLCD();
void disableCursorLCD();
void moveCursorLeftLCD();
void moveCursorRightLCD();
void setCursorIncrementFromLeftToRightLCD();
void setCursorIncrementFromRightToLeftLCD();

void moveDisplayLeftLCD();
void moveDisplayRightLCD();

void enableAutoScrollLCD();
void disableAutoScrollLCD();

void printLCDString(char *string);
void printfLCD(char *format, ...);
void printLCDChar(unsigned char charData);

void goToXYLCD(uint8_t row, uint8_t pos);
void printLCDStringAtPosition(uint8_t row, uint8_t pos, char *str);
void createCustomCharacter(uint8_t location, uint8_t *charmap);
void printLCDCustomCharacter(uint8_t location);

void initProgressBar(uint8_t len, uint8_t row, uint8_t col);
void incrementProgressBar();