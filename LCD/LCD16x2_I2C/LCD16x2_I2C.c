#include "LCD16x2_I2C.h"
#include <util/delay.h>
#include <stdarg.h>

static uint8_t LCD;
static uint8_t displayControlParameters;
static char formatBuffer[LCD_COL_COUNT + 1];

uint8_t loadBarStartElement[8] = { 0b10000, 0b11000, 0b11100, 0b11110, 0b11110, 0b11100, 0b11000, 0b10000 };
uint8_t loadBarProgressElement[8] = { 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111 };
uint8_t loadBarEndElement[8] = { 0b00001, 0b00011, 0b00111, 0b01111, 0b01111, 0b00111, 0b00011, 0b00001 };
	
uint8_t barRowLocation = 0;
uint8_t barColLocation = 0;
uint8_t barTotalLength = 0;
uint8_t barProgress = 0;


void initLCD() {
	initMasterI2C(I2C_PRESCALER_1);
	
	commandLCD(0x33);
	commandLCD(0x32);	// Send for 4 bit initialization of LCD
	commandLCD(LCD_TWO_LINES_5X8_MATRIX_4BIT_MODE);
	
	commandLCD(LCD_DISPLAY_ON_CURSOR_OFF);
	commandLCD(LCD_SHIFT_CURSOR_RIGHT);		// Auto Increment cursor from left to right
	clearLCD();
}

void commandLCD(uint8_t command) {
	startMasterI2C(DEVICE_ADDRESS);
	
	// send higher bits to LCD
	LCD = 0;
	LCD = LCD			
	| (command & 0xF0)  // get upper 4 bits
	| (1 << LCD_EN)		// enable pulse
	| (1 << LCD_BL);	// put unused pin to high
	
	LCD &= ~(1 << LCD_RS);	// explicitly set RS and RW to low for command
	LCD &= ~(1 << LCD_RW);
	writeMasterI2C(LCD);
	_delay_us(1);
	
	LCD &= ~(1 << LCD_EN);
	writeMasterI2C(LCD);
	_delay_us(200);
	
	
	// send lower bits to LCD
	LCD = 0;
	LCD = LCD
	| (command << 4)
	| (1 << LCD_EN)		// enable pulse
	| (1 << LCD_BL);
	
	LCD &= ~(1 << LCD_RS);	// explicitly set RS and RW to low for command
	LCD &= ~(1 << LCD_RW);
	writeMasterI2C(LCD);
	_delay_us(1);
	
	LCD &= ~(1 << LCD_EN);
	writeMasterI2C(LCD);
	_delay_ms(2);
	
	stopMasterI2C();
}

void printLCDChar(unsigned char charData) {
	startMasterI2C(DEVICE_ADDRESS);
	
	// send higher bits to LCD
	LCD = 0;
	LCD = LCD			 
	| (charData & 0xF0)  // get upper 4 bits
	| (1 << LCD_EN)		// enable pulse
	| (1 << LCD_RS)		// RS=1, data reg.
	| (1 << LCD_BL);	// put unused pin to high
	
	LCD &= ~(1 << LCD_RW);	// set RW low for data send
	writeMasterI2C(LCD);
	_delay_us(1);
	
	LCD &= ~(1 << LCD_EN);
	writeMasterI2C(LCD);
	_delay_us(200);
	
	
	// send lower bits to LCD
	LCD = 0;
	LCD = LCD			
	| (charData << 4) 
	| (1 << LCD_EN)
	| (1 << LCD_RS)		// RS=1, data reg.
	| (1 << LCD_BL);

	LCD &= ~(1 << LCD_RW);
	writeMasterI2C(LCD);
	_delay_us(1);
	
	LCD &= ~(1 << LCD_EN);
	writeMasterI2C(LCD);
	_delay_ms(2);
	
	stopMasterI2C();
}

void clearLCD() {
	commandLCD(LCD_CLEAR_DISPLAY);
	_delay_ms(2);		// Clear display command delay > 1.63 ms
	returnHomeLCD();
}

void turnOnLCD() {
	displayControlParameters |= LCD_DISPLAYON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void turnOffLCD() {
	displayControlParameters &= ~LCD_DISPLAYON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void returnHomeLCD() {
	commandLCD(LCD_MOVE_CURSOR_AT_FIRST_LINE_BEGINNING);
}

void enableBlinkingCursorLCD() {
	displayControlParameters |= LCD_DISPLAY_ON_CURSOR_BLINKING;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void disableBlinkingCursorLCD() {
	commandLCD(LCD_DISPLAY_ON_CURSOR_OFF);
}

void enableCursorLCD() {
	displayControlParameters |= LCD_CURSORON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void disableCursorLCD() {
	displayControlParameters &= ~LCD_CURSORON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void moveCursorLeftLCD() {
	commandLCD(LCD_SHIFT_POSITION_LEFT);
}

void moveCursorRightLCD() {
	commandLCD(LCD_SHIFT_POSITION_LEFT);
}

void moveCursorAtSecondLineBegginingLCD() {
	commandLCD(LCD_MOVE_CURSOR_AT_SECOND_LINE_BEGINNING);
}

void moveDisplayLeftLCD() {
	commandLCD(LCD_SHIFT_DISPLAY_LEFT);
}

void moveDisplayRightLCD() {
	commandLCD(LCD_SHIFT_DISPLAY_RIGHT);
}

void setCursotIncrementFromLeftToRightLCD() {
	displayControlParameters |= LCD_ENTRYLEFT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void setCursotIncrementFromRightToLeftLCD() {
	displayControlParameters &= ~LCD_ENTRYLEFT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void enableAutoScrollLCD() {
	displayControlParameters |= LCD_ENTRYSHIFTINCREMENT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void disableAutoScrollLCD() {
	displayControlParameters &= ~LCD_ENTRYSHIFTINCREMENT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void goToXYLCD(uint8_t row, uint8_t pos) {
	if (row == 0 && pos < LCD_COL_COUNT) {
		commandLCD((pos & 0x0F) | LCD_MOVE_CURSOR_AT_FIRST_LINE_BEGINNING);				// Command of first row and required position < 16
	} else if (row == 1 && pos < LCD_COL_COUNT) {
		commandLCD((pos & 0x0F) | LCD_MOVE_CURSOR_AT_SECOND_LINE_BEGINNING);			// Command of second row and required position < 16
	}
}

void printLCDString(char *string) {
	for (uint8_t i = 0; string[i] != 0; i++) {
		printLCDChar(string[i]);
	}
}

void printLCDStringAtPosition(uint8_t row, uint8_t pos, char *str) {			// Send string to LCD with xy position
	goToXYLCD(row, pos);
	printLCDString(str);
}

void createCustomCharacter(uint8_t location, uint8_t *charmap) {	// location from 0 to 7
	if (location < 8) {
		commandLCD(LCD_SETCGRAMADDR + (location * 8));	// Command 0x40 and onwards forces the device to point CGRAM address
		for (uint8_t i = 0; i < 8; i++) {
			printLCDChar(charmap[i]);
		}
		returnHomeLCD();
	}
}

void printLCDCustomCharacter(uint8_t location) {	// location from 0 to 7
	if (location < 8) {
		printLCDChar(location);
	}
}

void printfLCD(char *format, ...) {
	va_list args;

	va_start(args, format);
	vsnprintf(formatBuffer, LCD_COL_COUNT + 1, format, args);
	va_end(args);

	printLCDString(formatBuffer);
}

void initProgressBar(uint8_t len, uint8_t row, uint8_t col) {
	_Bool isBarLengthValid = (len <= LCD_COL_COUNT - 1) && ((len - 2) >= 0);
	_Bool isRowValid = (row == 0) || (row == 1);
	
	if (isBarLengthValid && isRowValid) {
		
		createCustomCharacter(0, loadBarStartElement);
		createCustomCharacter(1, loadBarProgressElement);
		createCustomCharacter(2, loadBarEndElement);
		
		barRowLocation = row;
		barColLocation = col + 1;
		barTotalLength = len - 1;
		
		goToXYLCD(row, col);
		printLCDCustomCharacter(0);
		
		goToXYLCD(row, (col + len));
		printLCDCustomCharacter(2);
		
	}
}

void incrementProgressBar() {
	if (barTotalLength != 0 && barProgress < barTotalLength) {
		goToXYLCD(barRowLocation, barColLocation++);
		printLCDCustomCharacter(1);
	}
	barProgress++;
}

