#pragma once

#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <avr/sfr_defs.h>

#include "debounce.h"
#include "Timer/Timer0.h"
#include "Labels/labels.h"

/************************************************************************

Header setup:
1. Define menu navigation buttons, or use defaults
2. By default menu list size is defined as 10 items, if need more change value of MENU_MAX_LENGTH

Example usage:
For creating such menu structure:

- Menu item 1
	- Sub menu 1
	- Sub menu 2
	- Sub menu 3
		- Sub sub menu 1
		- Sub sub menu 2
- Menu item 2
- Menu item 3
...

1. Initialize your display library
2. Call initEmptyMenu() -> 
	2.1 set your display clearing function pointer, for example: clearLCD()
	2.2 set your display string print function pointer, for example: printLCDString()
	2.3 set your display navigation pointer function with parameters as X and Y coordinates, for example: goToXYLCD()
3. Then create menu items calling createMenuItem() -> Example: MENU_ITEM *mainMenuItem1 = createMenuItem("MAIN MENU 1", testFunction);
	Where testFunction() - is working function when enter button is pressed on menu item, if menu item have sub menu items, then set it to NULL
4. Add main menu items as root menu, calling addRootMenuItem() -> where index - is order appearing items in menu
5. For adding sub menu items, call addSubMenuItem()

The result should be:
menuItem1 = createMenuItemWithSubMenus(name, 3);	<- provide sub menu number
menuItem2 = createMenuItemWithNoSubMenus(name, *funPointer1);
menuItem2 = createMenuItemWithNoSubMenus(name, *funPointer2);

subItem1 = createMenuItemWithNoSubMenus(name, *funPointer3);
subItem2 = createMenuItemWithNoSubMenus(name, *funPointer4);
subItem3 = createMenuItemWithSubMenus(name, 2);

subSubItem1 = createMenuItemWithNoSubMenus(name, *funPointer5);
subSubItem2 = createMenuItemWithNoSubMenus(name, *funPointer6);

addRootMenuItem(menuItem1, 0);
addRootMenuItem(menuItem2, 1);
addRootMenuItem(menuItem3, 2);

addSubMenuItem(menuItem1, subItem1, 0);
addSubMenuItem(menuItem1, subItem2, 1);
addSubMenuItem(menuItem1, subItem3, 2);

addSubMenuItem(subItem3, subSubItem1, 0);
addSubMenuItem(subItem3, subSubItem2, 0);

In your worker functions:
at the function end call -> backToMenu() - for returning back to menu

************************************************************************/

#define INITIAL_MAIN_MENU_ITEM_COUNT 6
#define MENU_MAX_ITEM_VIEW 2

typedef void (*FuncPtr)();

typedef struct NaviItem {
	uint8_t upperCursor;
	uint8_t lowerCursor;
} NaviItem;

typedef struct MenuItem {							// Data size if taken from: https://gcc.gnu.org/wiki/avr-gcc
	Labels label;								    // 1 byte after packaging or 2 by default
	uint8_t subMenuCount;						    // 1 byte
	FuncPtr functionPointer;						// 2 bytes
	NaviItem naviCursor;						    // 2 bytes
	struct MenuItem *previousMenu;				    // 2 bytes		
	struct MenuItem *nextMenu[];					// size * 2 bytes
} __attribute__((packed, aligned(1))) MenuItem;		// Total by default: 8 bytes

void setupMenu( void (*displayClearFunctPointer)(), void (*displayStringFunctPointer)(char*), void (*goToXYFunPointer)(uint8_t, uint8_t));
void displayMenu();
void backToMenu();

MenuItem * createMenuItemWithSubMenus(Labels label, uint8_t subMenuCount);
MenuItem * createMenuItemWithNoSubMenus(Labels label, FuncPtr functionPtr);
void addRootMenuItem(MenuItem *menuItem, uint8_t index);
void addSubMenuItem(MenuItem *menuItem, MenuItem *subMenuItem, uint8_t index);

void enableMenuArrowPointerAtPosition(uint8_t col, uint8_t cursorSimbol, void (*displayCustomSymbolFuncPointer)(uint8_t));
void setBackFunction( void (*backFunctPointer)());