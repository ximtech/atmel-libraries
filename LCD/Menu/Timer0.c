#include "Timer0.h"
#include <avr/interrupt.h>


void initTimer0(uint8_t prescaler, uint8_t initialValue) {
	if (prescaler > 5) {				//if an external clock source is used, set T0 pin as input
		DDRB &= ~(1 << PORTB0);
	}
	resetTimer0();
	setTimer0(initialValue);
	TCCR0 &= ~((1 << WGM01) | (1 << WGM00));		// set normal mode
	TCCR0 = prescaler & 0x07;        //set the prescaler and mask bits that aren't the prescaler
}

uint8_t getTimerValue() {
	return TCNT0;                     //return the timer counter 0 register value
}

void setTimer0(uint8_t value) {
	TCNT0 = value;                       //set the timer0 register
}

void resetTimer0() {
	TCNT0 = 0;                       //set the timer0 register to 0, clearing the timer
	TIFR = 0x1;
}

void enableInterruptTimer0() {
	TIMSK = (1 << TOIE0);            //set the timer overflow interrupt enable 0 bit
	sei();                            //set the global interrupt flag
}

void disableInterruptTimer0() {
	TIMSK &= ~(1 << TOIE0);           //clear the timer overflow interrupt enable 0 bit
}
