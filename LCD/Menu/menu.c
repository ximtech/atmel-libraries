#include "menu.h"

#define NO_SUB_MENUS 0

static void (*clearDisplayFunction)() = NULL;
static void (*displayStringFunction)(char*) = NULL;
static void (*goToXYFunction)(uint8_t, uint8_t) = NULL;
static void (*displayArrowPointerFunction)(uint8_t) = NULL;
static void (*goBackFunction)() = NULL;

static struct DisplayOptions {
	_Bool isMenuAlreadyViewed;
	_Bool isArrowCursorEnabled;
	uint8_t arrowCursorSimbol;
	uint8_t arrowCursorColumn;
} displayOptions;

static MenuItem *entryPoint = NULL;

ISR(TIMER0_OVF_vect) {
	
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		if (entryPoint -> naviCursor.upperCursor > 0) {
			entryPoint -> naviCursor.upperCursor--;
			entryPoint -> naviCursor.lowerCursor--;
			displayOptions.isMenuAlreadyViewed = false;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
	}
	
	if (button_down(BUTTON_DOWN_MASK)) {
		if ((entryPoint -> naviCursor.lowerCursor) <= (entryPoint -> subMenuCount)) {
			entryPoint -> naviCursor.upperCursor++;
			entryPoint -> naviCursor.lowerCursor++;
			displayOptions.isMenuAlreadyViewed = false;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
	}
	
	if (button_down(BUTTON_ENTER_MASK)) {
		MenuItem *tmpMenuItem = entryPoint -> nextMenu[entryPoint -> naviCursor.upperCursor];
		
		if (tmpMenuItem -> subMenuCount > 0) {
			entryPoint = tmpMenuItem;
			entryPoint -> naviCursor.upperCursor = 0;
			entryPoint -> naviCursor.lowerCursor = MENU_MAX_ITEM_VIEW;
			displayOptions.isMenuAlreadyViewed = false;

		} else if (tmpMenuItem -> functionPointer != NULL) {
			disableInterruptTimer0();
			tmpMenuItem -> functionPointer();
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
	}
	
	if (button_down(BUTTON_BACK_MASK)) {
		if (entryPoint -> previousMenu != NULL) {
			entryPoint = entryPoint -> previousMenu;
			displayOptions.isMenuAlreadyViewed = false;
		} else if (goBackFunction != NULL) {
			entryPoint -> naviCursor.upperCursor = 0;
			entryPoint -> naviCursor.lowerCursor = MENU_MAX_ITEM_VIEW;
			displayOptions.isMenuAlreadyViewed = false;
			disableInterruptTimer0();
			goBackFunction();
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
	
}

static void setClearDisplayFunction(void (*displayClearFunctPointer)()) {
	clearDisplayFunction = displayClearFunctPointer;
}

static void setMenuItemDisplayFunction(void (*displayFunctPointer)(char*)) {
	displayStringFunction = displayFunctPointer;
}

static void setGoToXYFunction(void (*goToXYFunPointer)(uint8_t, uint8_t)) {
	goToXYFunction = goToXYFunPointer;
}

static void printMenuItem(uint8_t index) {
	if (index == entryPoint -> subMenuCount) {
		goToXYFunction((index - entryPoint -> naviCursor.upperCursor), 0);
		displayStringFunction(" ");
	} else {
		goToXYFunction((index - entryPoint -> naviCursor.upperCursor), 0);
		displayStringFunction(getLabelString( entryPoint->nextMenu[index] -> label ));
	}
}

static void printMenuItemList(uint8_t menuCount) {
	for (uint8_t i = entryPoint -> naviCursor.upperCursor; i < menuCount; i++) {
		printMenuItem(i);
	}
}

void displayMenu() {
	
	if (entryPoint->subMenuCount > 0 && !displayOptions.isMenuAlreadyViewed) {
		disableInterruptTimer0();
		clearDisplayFunction();
		
		if (displayOptions.isArrowCursorEnabled) {
			goToXYFunction(0, displayOptions.arrowCursorColumn);
			displayArrowPointerFunction(displayOptions.arrowCursorSimbol);
		}
		
		if (entryPoint->subMenuCount >= MENU_MAX_ITEM_VIEW) {
			printMenuItemList(entryPoint -> naviCursor.lowerCursor);
		} else {
			printMenuItemList(entryPoint->subMenuCount);
		}
		
		displayOptions.isMenuAlreadyViewed = true;
		enableInterruptTimer0();
	}
	
}

static MenuItem * createMenuItem(Labels label, uint8_t subMenuCount, FuncPtr functionPtr) {
	uint8_t subMenuItemSize = subMenuCount > 0 ? sizeof(MenuItem[subMenuCount]) : 0;
	MenuItem *menuItem = (struct MenuItem*) malloc( sizeof(MenuItem) + subMenuItemSize );
	
	if (menuItem == NULL) {
		if (displayStringFunction != NULL) {
			displayStringFunction(getLabelString(L_NO_MEMORY));
			while(true) {}	// loop infinitely, there no reason to proceed program flow
		}
		return menuItem;
	}
	
	menuItem -> subMenuCount = subMenuCount;
	menuItem -> label = label;
	menuItem -> functionPointer = functionPtr;
	menuItem -> naviCursor.upperCursor = 0;
	menuItem -> naviCursor.lowerCursor = MENU_MAX_ITEM_VIEW;
	return menuItem;
}

MenuItem * createMenuItemWithNoSubMenus(Labels label, FuncPtr functionPtr) {
	return createMenuItem(label, NO_SUB_MENUS, functionPtr);
}

MenuItem * createMenuItemWithSubMenus(Labels label, uint8_t subMenuCount) {
	return createMenuItem(label, subMenuCount, NULL);
}

void addRootMenuItem(MenuItem *menuItem, uint8_t index) {
	entryPoint -> nextMenu[index] = menuItem;
	menuItem -> previousMenu = entryPoint;
}

void addSubMenuItem(MenuItem *menuItem, MenuItem *subMenuItem, uint8_t index) {
	menuItem -> nextMenu[index] = subMenuItem;
	subMenuItem -> previousMenu = menuItem;
}

void setupMenu( void (*displayClearFunctPointer)(), void (*displayStringFunctPointer)(char*), void (*goToXYFunPointer)(uint8_t, uint8_t) ) {
	debounce_init();

	displayOptions.isArrowCursorEnabled = false;
	displayOptions.isMenuAlreadyViewed = false;
	
	entryPoint = createMenuItemWithSubMenus(L_ENTRY_POINT, INITIAL_MAIN_MENU_ITEM_COUNT);
	entryPoint -> previousMenu = NULL;
	entryPoint -> naviCursor.upperCursor = 0;
	entryPoint -> naviCursor.lowerCursor = MENU_MAX_ITEM_VIEW;
	
	setClearDisplayFunction(displayClearFunctPointer);
	setMenuItemDisplayFunction(displayStringFunctPointer);
	setGoToXYFunction(goToXYFunPointer);
	
	initTimer0(TIMER0_PRESCALER_1024, 0);
}

void backToMenu() {
	displayOptions.isMenuAlreadyViewed = false;
	displayMenu();
}

void enableMenuArrowPointerAtPosition(uint8_t col, uint8_t cursorSimbol, void (*displayCustomSymbolFuncPointer)(uint8_t)) {
	displayArrowPointerFunction = displayCustomSymbolFuncPointer;
	displayOptions.arrowCursorSimbol = cursorSimbol;
	displayOptions.arrowCursorColumn = col;
	displayOptions.isArrowCursorEnabled = true;
}

void setBackFunction(void (*backFunctPointer)()) {
	goBackFunction = backFunctPointer;
}
