#pragma once

#include <avr/pgmspace.h>

typedef enum __attribute__ ((__packed__)) Labels {
	// Menu labels
	L_MAIN_MENU,
	L_SET_DATE_TIME,
	L_SET_ZONE_1,
	L_SET_ZONE_2,
	L_SET_ZONE_3,
	L_SET_ZONE_4,
} Labels;  // pack enum from 2 to 1 byte

char * getLabelString(Labels label);


