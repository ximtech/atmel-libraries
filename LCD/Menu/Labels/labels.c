#include "labels.h"


// Menu labels
const char PROGMEM LABEL_MAIN_MENU[] = "   - MENU -     ";
const char PROGMEM LABEL_SET_DATE_TIME[] = " Set Date/Time ";
const char PROGMEM LABEL_SET_ZONE_1[] = " Setup Zone 1 ";
const char PROGMEM LABEL_SET_ZONE_2[] = " Setup Zone 2 ";
const char PROGMEM LABEL_SET_ZONE_3[] = " Setup Zone 3 ";
const char PROGMEM LABEL_SET_ZONE_4[] = " Setup Zone 4 ";


const char * const labelTable[] PROGMEM = {
	LABEL_MAIN_MENU,
	LABEL_SET_DATE_TIME,
	LABEL_SET_ZONE_1,
	LABEL_SET_ZONE_2,
	LABEL_SET_ZONE_3,
	LABEL_SET_ZONE_4
};

static char buffer[30];

char * getLabelString(Labels label) {
	strcpy_P(buffer, (char *)pgm_read_word( &labelTable[label] ));
	return buffer;
}
