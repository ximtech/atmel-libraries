#include "ADC.h"
#include <util/delay.h>
#include <avr/sfr_defs.h>

void initADC() {
	ADC_DDR = 0x00;										 // ADC ports to input
	ADMUX |= (1 << REFS0);								// by default set voltage reference to AVCC
	ADCSRA = (1 << ADEN) | (1 << ADSC) | ADC_PRESCALE;  // enable the ADC converter
	loop_until_bit_is_set(ADCSRA, ADSC);				// get first measurement
}

uint16_t getADC10BitValue(uint8_t channel) {
	ADMUX  |= (channel & 0x0f);			  // set up the multiplexer to select desired ADC pin
	ADCSRA |=  (1 << ADSC);               // start the conversion
	_delay_us(64);                        // wait 64 microseconds for the flag to register that its not done
	loop_until_bit_is_set(ADCSRA, ADSC);
	return ADC;                           // return the 10 bit ADC value
}

uint8_t getADC8BitValue (uint8_t channel) {
	uint16_t adc10bitValue = getADC10BitValue(channel);
	return (adc10bitValue >>= 2) & 0xFF;
}

