#include "AnalogComp.h"


void initAnalogComparator() {
	SFIOR |= (1 << ACME);					// enable analog comparator
	ACSR = 0x00;							// clear ACSR resister
}

void initAnalogComparatorFromAdc(uint8_t channel) {
	ADCSRA &= (1 << ADEN);					// disable ADC
	ADMUX = channel & 0x07;                 // the the desired channel to set to the negative input and mask unused bits
	SFIOR |= (1 << ACME);					// enable analog comparator
	ACSR = 0x00;							// clear ACSR resister
}

void switchChannel(uint8_t channel) { //switched the negative input pin of the Analog comparator in multiplexer mode
	ADMUX = channel & 0x07;     //set the multiplexer channel and mask unused buts
	__asm("NOP");              //wait 2 clock cycles
	__asm("NOP");
}

uint8_t getAnalogCompValue() {
	return ((ACSR >> ACO) & 1);             //return the ACO (analog comparator output) bit in ACSR
}
