#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>


void initAnalogComparator();

void initAnalogComparatorFromAdc(uint8_t channel);

void switchChannel(uint8_t channel);

uint8_t getAnalogCompValue();