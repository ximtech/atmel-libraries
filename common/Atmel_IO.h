/*
 AtmelMacros Library - input/output library for AVR 8-bit microcontrollers.
   The idea of this library is taken from Arduino project. Every pin of the microcontroller
   has unique 8-bit number given by constants ioPB0, ioBP1, ioPD0, etc. In your program you use
   functions digitalRead, digitalWrite for working with these pins.
   The main benefit is you don't need to worry about I/O registers.
*/

#pragma once

#include <avr/io.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <avr/eeprom.h>
#include "AtmelPins.h"

#ifdef IO_REG16
typedef volatile uint16_t IOReg;
#else
typedef volatile uint8_t IOReg;
#endif // IO_REG16


/* Pin types */
#define INPUT 0
#define OUTPUT 1

/* Logical state values */
#define HIGH 1
#define LOW  0

#define PI 3.1416
#define HALF_PI 1.57
#define TWO_PI 6.283
#define DEG_TO_RAD 0.0174533
#define RAD_TO_DEG 57.2958

#define MIN(a, b)     ({ typeof (a) _a = (a); typeof (b) _b = (b); _a < _b ? _a : _b; })
#define MAX(a, b)     ({ typeof (a) _a = (a); typeof (b) _b = (b); _a > _b ? _a : _b; })
#define ROUND(x)      ({ typeof (x) _x = (x); _x >= 0 ? (long)(_x + 0.5) : (long)(_x - 0.5); })
#define RADIANS(deg)  ((deg) * DEG_TO_RAD)
#define DEGREES(rad)  ((rad) * RAD_TO_DEG)
#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))
#define IS_IN_RANGE(value, min, max) ((value) >= (min) && (value) <= (max) ? true : false)


//----- I/O Macros -----
//Macros to edit PORT, DDR and PIN
#define PIN_MODE(pin, mode)        ((mode) == OUTPUT ? (SET_PINS_FOR_REG(DDR, SET, pin)) : (SET_PINS_FOR_REG(DDR, CLEAR, pin)))
#define DIGITAL_WRITE(pin, mode)   ((mode) == HIGH ? (SET_PINS_FOR_REG(PORT, SET, pin)) : (SET_PINS_FOR_REG(PORT, CLEAR, pin)))
#define DIGITAL_READ(pin)          ((ioPIN(pin) >> digitalPinToBit(pin)) & 0x01)
#define PIN_MODE_TOGGLE(pin)       (SET_PINS_FOR_REG(DDR, TOGGLE, pin))
#define DIGITAL_LEVEL_TOGGLE(pin)  (SET_PINS_FOR_REG(PORT, TOGGLE, pin))


//General use bit manipulating commands
#define bitRead(value, bit)  (((value) >> (bit)) & 0x01)
#define bitSet(value, bit)   ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitValue) ((bitValue) ? bitSet(value, bit) : bitClear(value, bit))
#define bitToggle(value, bit) ((value) ^=    (1UL << (bit)))


/*
Following functions are compatible with Arduino functions
except of different pin values
Pin number is format PPPPPBBB, where
   PPPPP is port number
     BBB is bit number
 */
#define digitalPinToBit(pin) ((pin) & 0x07)
#define digitalPinToBitMask(pin) _BV(digitalPinToBit(pin))
#define digitalPinToPort(pin) ((pin) >> 3)

/* Returns address of register PORT , DDR or PIN for number pin
   For example:
        PORTB == ioPORT(ioPB1)
        DDRC  == ioDDR(ioPC2)
        PIND  == ioPIN(ioPD3)
*/
#define ioPORT(pin) CAT(PORT_AT_P, pin)
#define ioDDR(pin) CAT(DDR_AT_P, pin)
#define ioPIN(pin) CAT(PIN_AT_P, pin)

/*
  Return true expression if port1 == port2, otherwiese returns
  false expression.
  Input parameters port1, port2 are constant values ioPORTA, ioPORTB, ...
*/

#define IIF_PORT_EQ(port1, port2, true, false)  \
    CAT(                                        \
        CAT(_IIF_PORT_EQ_, port1),              \
        CAT(_, port2)                           \
    ) (true, false)


/* Return port number for given pin */
#define PIN_TO_IOPORT(pin) CAT(IOPORT_AT_P, pin)


#define SET_IOSET_BITS0(reg)
#define CLEAR_IOSET_BITS0(reg)
#define TOGGLE_IOSET_BITS0(reg)

#define SET_IOSET_BITS1(reg, pin) reg |= digitalPinToBitMask(pin)
#define CLEAR_IOSET_BITS1(reg, pin) reg &= ~digitalPinToBitMask(pin)
#define TOGGLE_IOSET_BITS1(reg, pin) reg ^= digitalPinToBitMask(pin)


/*
  Set/clear bits inside the register reg
  Input parameters:
     mode - value SET, CLEAR
     reg  - PORT, PIN, DDR
     ...  - pins for example ioPB1, ioPB2 ...
            or bit numbers 0..7
*/
#define ioSET_BITS(mode, ...)               \
    GET_MACRO2(__VA_ARGS__,                 \
               mode##_IOSET_BITS1,          \
               mode##_IOSET_BITS0)(__VA_ARGS__)


#define SET_PINS_FOR_REG_ARGUMENT(port_name, pin)                                \
    ADD_ARGUMENT(                                                             \
        IS_NOT_EMPTY(pin,                                                     \
            IIF_PORT_EQ(ioPORT##port_name, PIN_TO_IOPORT(pin), pin, EMPTY())  \
        )                                                                     \
    )

/*
  Input parameters:
    reg         - expected values: PORT, PIN, DDR
    port_name   - expected values: A, B, C, D, E, ...
    mode        - expected values: CLEAR, SET
*/
#define SET_PINS_FOR_REG_PORT_ITEM(reg, port_name, mode, pin) \
    IIF_PORT##port_name( \
        DEFER(ioSET_BITS)(mode, reg##port_name SET_PINS_FOR_REG_ARGUMENT(port_name, pin)), EMPTY())


#define SET_PINS_FOR_REG(reg, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, A, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, B, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, C, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, D, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, E, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, F, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, G, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, H, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, I, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, J, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, K, mode, pin) \
        SET_PINS_FOR_REG_PORT_ITEM(reg, L, mode, pin) \

/*
    The following function are simillar to ioPIN, ioDDR, ioPORT
    The main differences are:
       - the functions return pointers
       - the input value is port number (result of digitalPinToPort).
       - functions cab be use in runtime. Input value can be variable not constant value.

    We can write:
        & ioPORT(ioPB2) == portOutputRegister(digitalPinToPort(ioPB2))

    portInputRegister   - return register PIN address
    portModeRegister    - return register DDR address
    portOutputRegister  - return register PORT address
*/
#ifdef IO_REG16
    #define portInputRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_word( &portToInputPGM[(port)] )) )
    #define portModeRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_word( &portToInputPGM[(port)]) + 1) )
    #define portOutputRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_word( &portToInputPGM[(port)]) + 2) )
#else
    #define portInputRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_byte( &portToInputPGM[(port)])) )
    #define portModeRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_byte( &portToInputPGM[(port)]) + 1) )
    #define portOutputRegister(port) ( (volatile IOReg *)(uint16_t) (pgm_read_byte( portToInputPGM + (port)) + 2) )
#endif // IO_REG16

static const IOReg portToInputPGM[PORT_HIGH_INDEX + 1] PROGMEM = {
        PORT_TO_INPUT_TABLE()
};

static inline void pinMode(uint8_t pin, uint8_t mode) {
    uint8_t bit = digitalPinToBitMask(pin);
    uint8_t port = digitalPinToPort(pin);
    IOReg *reg = portModeRegister(port);
    IOReg *out= portOutputRegister(port);

    if (mode == INPUT) {
        *reg &= ~bit;
        *out &= ~bit;
    } else {
        *reg |= bit;
    }
}

static inline void digitalWrite(uint8_t pin, uint8_t val) {
    uint8_t bit = digitalPinToBitMask(pin);
    uint8_t port = digitalPinToPort(pin);
    IOReg *out = portOutputRegister(port);

    if (val == LOW) {
        *out &= ~bit;
    } else {
        *out |= bit;
    }
}

static inline uint8_t digitalRead(uint8_t pin) {
    uint8_t bit = digitalPinToBitMask(pin);
    uint8_t port = digitalPinToPort(pin);
    return (*portInputRegister(port) & bit) ? HIGH : LOW;
}


// Private Macros
#define EMPTY()
#define DEFER(id) id EMPTY()
#define ADD_ARGUMENT(...) ADD_ARGUMENT_IMPL(__VA_ARGS__)
#define ADD_ARGUMENT_IMPL(...) EMPTY(), ## __VA_ARGS__
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define GET_MACRO2(_1, _2, NAME, ...) NAME

/* If expr is not EMPTY return value */
#define IS_NOT_EMPTY(expr, value) IS_NOT_EMPTY_BASE(value ADD_ARGUMENT(expr))
#define IS_EMPTY_RETURN_VALUE(value, expr) value
#define IS_EMPTY_RETURN_EMPTY(value)
#define IS_NOT_EMPTY_BASE(...)                     \
    GET_MACRO2(__VA_ARGS__,                     \
               IS_EMPTY_RETURN_VALUE,          \
               IS_EMPTY_RETURN_EMPTY,          \
               )(__VA_ARGS__)

/* Return 1 if macro given by its name is defined and it is empty.
 * Return 0 if macro is not defined at all or it is defined with some value.
 */
#define IS_EMPTY_DEF(name) \
  CAT(IS_EMPTY_DEF_, IS_NOT_EMPTY(name, 0))()

#define IS_EMPTY_DEF_()  1
#define IS_EMPTY_DEF_0() 0