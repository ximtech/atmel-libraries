#include "SPI.h"

static void setSPISpeed(enum SpiSCK speed) {
	uint8_t speedValue = 0;
	switch(speed) {
		case FOSC_2: speedValue = 4;
		break;
		case FOSC_4: speedValue = 0;
		break;
		case FOSC_8: speedValue = 5;
		break;
		case FOSC_16: speedValue = 1;
		break;
		case FOSC_32: speedValue = 6;
		break;
		case FOSC_64: speedValue = 2;
		break;
		case FOSC_128: speedValue = 3;
		break;
	};
	SPCR |= (speedValue >> 2) & 0x01;
};

void initSpiAsMaster(enum SpiSCK speed) {
	SPI_DDR |= (1 << MOSI) | (1 << SCK) | (1 << SS);		// set MOSI, SS and SCK as outputs
	SPI_PORT &= ~((1 << MOSI) | (1 << SCK) | (1 << SS));	// set MOSI, SS and SCK to low
	SPI_DDR &= ~(1 << MISO);								// set MISO as an input
	SPI_PORT |= (1 << SS);									// disable slave initially by making high on SS pin
	setSPISpeed(speed);
	SPSR &= ~(1 << SPI2X);									// disable speed doubler
	SPCR |= (1 << SPE) | (1 << MSTR);						// enable SPI in master mode
}

void initSpiAsSlave() {
	SPI_DDR &= ~((1 << MOSI) | (1 << SCK) | (1 << SS));	// make MOSI, SCK, SS pin direction as input pins
	SPI_DDR |= (1 << MISO);								// make MISO pin as output pin
	SPCR &= ~(1 << MSTR);
	SPI_PORT &= ~(1 << SS);								// define Slave enable
	SPCR |= (1 << SPE);									// enable SPI
}

uint8_t sendCharSPI(char byte) {
	SPDR = byte;                         // set the data byte and start the transfer
	while (!(SPSR & (1 << SPIF)));       // wait for the transfer to complete
	return SPDR;                         // return received data
}

char receiveCharSPI() { 
	while (!(SPSR & (1 << SPIF)));				// wait till reception complete
	return SPDR;								// return received data
}