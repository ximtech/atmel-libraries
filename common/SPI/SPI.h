#pragma once

#include <avr/io.h>
#include <stdint.h>

// define SPI bus pins
#define SPI_DDR DDRB
#define SPI_PORT PORTB

#define MOSI PB3							
#define MISO PB4
#define SCK  PB5
#define SS   PB2

//clock speeds
enum SpiSCK {
	FOSC_2,
	FOSC_4,
	FOSC_8,
	FOSC_16,
	FOSC_32,
	FOSC_64,
	FOSC_128,
};

void initSpiAsMaster(enum SpiSCK speed);
void initSpiAsSlave();
uint8_t sendCharSPI(char byte);
char receiveCharSPI();