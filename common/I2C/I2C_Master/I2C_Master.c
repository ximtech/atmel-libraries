#include "I2C_Master.h"
#include <util/twi.h>
#include <avr/sfr_defs.h>


static void setPrescalerValue(i2cPrescaler prescaler) {
	uint8_t prescalerValue = 0;
	switch(prescaler) {
		case I2C_PRESCALER_1: break;
		case I2C_PRESCALER_4: 
		prescalerValue = 1;
		break;
		case I2C_PRESCALER_16: 
		prescalerValue = 2;
		break;
		case I2C_PRESCALER_64: 
		prescalerValue = 3;
		break;
	};
	TWSR = (prescalerValue & 0x03);			// set the prescaler
};

void initMasterI2C(i2cPrescaler prescaler) {
	TWBR = ((F_CPU / SCL_CLOCK) - 16) / 2;  // must be > 10 for stable operation 
	setPrescalerValue(prescaler);
}

i2cMasterStatus startMasterI2C(uint8_t address) {
	uint8_t status = 0;								// create a buffer variable
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);		// enable TWI, generate START
	loop_until_bit_is_set(TWCR, TWINT);				// wait until complete					
	
	status = TWSR & 0xF8;										// read status, masking the prescaler bits
	if ((status != TW_START) && (status != TW_REP_START)) {		// if start not completed, return error
		return I2C_START_ERROR;		
	}
	
	TWDR = address;								// set the data register to the address
	TWCR = (1 << TWINT) | (1 << TWEN);         // transmit the address
	loop_until_bit_is_set(TWCR, TWINT);		  // wait until complete	
	
	status = TW_STATUS & 0xF8;										// read status, masking the prescaler bits
	if ((status != TW_MT_SLA_ACK) && (status != TW_MR_SLA_ACK)) {	// if ack not received, return error
		return I2C_ACK_ERROR;
	}
	return OK;
}

void stopMasterI2C() {
	TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);  // transmit stop condition
	loop_until_bit_is_clear(TWCR, TWSTO);			  // wait for it to complete                  
}

i2cMasterStatus writeMasterI2C(char dataByte) {
	TWDR = dataByte;                            // set up the data
	TWCR = (1 << TWINT) | (1 << TWEN);          // start the transmission
	loop_until_bit_is_set(TWCR, TWINT);         // wait until complete
	
	uint8_t status = TWSR & 0xF8;              // get the status
	if (status == TW_MT_DATA_ACK) {	
		return OK;       
	}
	
	if (status == TW_MT_DATA_NACK) {
		return I2C_NACK_RECEIVED;      
	}
	return I2C_DATA_TX_ERROR;
}

char readMasterI2C() {
	TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWEN);	// start read with Ack
	loop_until_bit_is_set(TWCR, TWINT);					// wait until complete
	return TWDR;										// return data
}

char readMasterNackI2C() {
	TWCR = (1 << TWINT) | (1 << TWEN);          // start read with Nack
	loop_until_bit_is_set(TWCR, TWINT);			// wait until complete
	return TWDR;                                // return data
}