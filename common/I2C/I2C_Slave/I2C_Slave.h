#pragma once

#include <avr/io.h>
#include "I2C_Slave.h"

/*
* Slave I2C usage:
* For sending data:
*	1. initSlaveI2C(uint8_t slaveAddress, enum i2cPrescaler prescaler) - set slave address and set value from i2cPrescaler enum
*	2. slaveListenI2C() - listen for master command, 
*			if response is DATA_ACK_RECEIVE: then master is ready to receive data. 
*			If DATA_ACK_SEND: then slave can send data to master
*	3. slaveSendI2C(char data) - when master is ready to receive data, then byte of data can be send. For response see: i2cSlaveDataStatus with status *_SEND_*
*	4. slaveReceiveI2C() - when master is ready to send data, then byte of data can be received. Check response with: i2cSlaveDataStatus with status *_RECEIVE_*
*/

//#define F_CPU 8000000UL

#ifndef F_CPU
#error "F_CPU is not defined in I2C_Slave.h"
#endif

// I2C clock in Hz
#define SCL_CLOCK  100000L

typedef enum i2cSlaveStatus {
	DATA_ACK_RECEIVE,
	DATA_ACK_SEND,
	GENERAL_ACK
} i2cSlaveStatus;

typedef enum i2cSlaveDataStatus {
	DATA_SEND_OK,
	DATA_SEND_STOP,
	DATA_SEND_NACK_RECEIVED,
	DATA_SEND_LAST,
	DATA_SEND_UNKNOWN_STATUS,
	
	DATA_RECEIVE_STOP = -1,
	DATA_RECEIVE_UNKNOWN_STATUS = -2
} i2cSlaveDataStatus;

typedef enum i2cPrescaler {
	I2C_PRESCALER_1,
	I2C_PRESCALER_4,
	I2C_PRESCALER_16,
	I2C_PRESCALER_64
} i2cPrescaler;

void initSlaveI2C(uint8_t slaveAddress, i2cPrescaler prescaler);
i2cSlaveStatus slaveListenI2C();
i2cSlaveDataStatus slaveSendI2C(char data);
char slaveReceiveI2C();