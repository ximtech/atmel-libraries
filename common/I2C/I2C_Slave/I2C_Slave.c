#include "I2C_Slave.h"
#include <util/twi.h>
#include <avr/sfr_defs.h>


static void setPrescalerValue(i2cPrescaler prescaler) {
	uint8_t prescalerValue = 0;
	switch(prescaler) {
		case I2C_PRESCALER_1: break;
		case I2C_PRESCALER_4:
		prescalerValue = 1;
		break;
		case I2C_PRESCALER_16:
		prescalerValue = 2;
		break;
		case I2C_PRESCALER_64:
		prescalerValue = 3;
		break;
	};
	TWSR = prescalerValue & 0x07;//set the i2c prescaler, masking non used bits
};

void initSlaveI2C(uint8_t slaveAddress, i2cPrescaler prescaler) {
	TWAR = slaveAddress & 0xfe;						// set the slave address, masking direction bit
	TWBR = ((F_CPU / SCL_CLOCK) - 16) / 2;			// must be > 10 for stable operation 
	TWCR = (1 << TWEN) | (1 <<TWEA) | (1 << TWINT);  // enable TWI, Enable Ack generation
	setPrescalerValue(prescaler);
}

i2cSlaveStatus slaveListenI2C() {
	while(1) {
		uint8_t status;
		loop_until_bit_is_set(TWCR, TWINT);	 // wait to be addressed
		status = TWSR & 0xF8;				 // read TWI status register
		
		switch(status) {
			case TW_SR_SLA_ACK:
			case TW_SR_ARB_LOST_SLA_ACK: 
				return DATA_ACK_RECEIVE;	// own SLA+W received, Ack returned, then slave need to receive data sent by the master.
				
			case TW_ST_SLA_ACK:
			case TW_ST_ARB_LOST_SLA_ACK:
				return DATA_ACK_SEND;		// own SLA+R received, Ack returned, then slave must send the requested data
				
			case TW_SR_GCALL_ACK:
			case TW_SR_ARB_LOST_GCALL_ACK:
				return GENERAL_ACK;				// general call received, Ack returned
				
			default: 
				continue;
		}
	}
}

i2cSlaveDataStatus slaveSendI2C(char data) {
	uint8_t status;
	TWDR = data;										// write data to TWDR to be transmitted
	TWCR = (1 << TWEN) | (1 << TWINT) | (1 << TWEA);	// enable TWI & clear interrupt flag
	loop_until_bit_is_set(TWCR, TWINT);					// wait until TWI finish its current job
	status = TWSR & 0xF8;								// read TWI status register
	
	switch(status) {
		case TW_SR_STOP:		// check for STOP/REPEATED START received
			TWCR |= (1 << TWINT);	// clear interrupt flag
			return DATA_SEND_STOP;
			
		case TW_ST_DATA_ACK:		// check for data transmitted, Ack received
			return DATA_SEND_OK;
			
		case TW_ST_DATA_NACK:		// check for data transmitted, Nack received
			TWCR |= (1 << TWINT);
			return DATA_SEND_NACK_RECEIVED;
			
		case TW_ST_LAST_DATA:		// Last byte transmitted with Ack received
			return DATA_SEND_LAST;
		
		default: 
			return DATA_SEND_UNKNOWN_STATUS;
	}
}

char slaveReceiveI2C() {
	uint8_t status;
	TWCR = (1 << TWEN) | (1 << TWEA) | (1 << TWINT);	// enable TWI & generation of Ack
	loop_until_bit_is_set(TWCR, TWINT);					// wait until TWI finish its current job
	status = TWSR & 0xF8;								// read TWI status register
	
	switch(status) {
		case TW_SR_DATA_ACK:			// check for data received, Ack returned
		case TW_SR_GCALL_DATA_ACK:
		case TW_SR_DATA_NACK:			// check for data received, Nack returned & switched to not addressed slave mode
		case TW_SR_GCALL_DATA_NACK:
			return TWDR;				// if yes then return received data
			
		case TW_SR_STOP:				// check whether STOP/REPEATED START
			TWCR |= (1 << TWINT);		// clear interrupt flag & return -1
			return DATA_RECEIVE_STOP;
			
		default: 
		return DATA_RECEIVE_UNKNOWN_STATUS;
	}
}