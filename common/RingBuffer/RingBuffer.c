#include "RingBuffer.h"

struct RingBuffer {
	uint16_t head;
	uint16_t tail;
	uint16_t maxSize;
	uint8_t *buffer;
	bool isFull;
};

static void advanceBufferPointer(RingBufferPointer ringBuffer);
static void retreatBufferPointer(RingBufferPointer ringBuffer);


RingBufferPointer getRingBufferInstance(uint16_t bufferSize) {
	if (bufferSize < 1) {
		return NULL;
	}
	RingBufferPointer instance = malloc(sizeof(struct RingBuffer));
	uint8_t *buffer = malloc(sizeof(uint8_t) * bufferSize);
	
	if (instance != NULL && buffer != NULL) {
		instance->buffer = buffer;
		instance->maxSize = bufferSize;
		reset(instance);
		return instance;
	}
	return NULL;
}

void reset(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		memset(ringBuffer->buffer, 0, ringBuffer->maxSize);
		ringBuffer->head = 0;
		ringBuffer->tail = 0;
		ringBuffer->isFull = false;
	}
}

void delete(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		free(ringBuffer);
	}
}

bool isFull(RingBufferPointer ringBuffer) {
	return (ringBuffer != NULL) ? ringBuffer->isFull : true;
}

bool isNotFull(RingBufferPointer ringBuffer) {
	return !isFull(ringBuffer);
}

bool isEmpty(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			return !ringBuffer->isFull && (ringBuffer->head == ringBuffer->tail);
		}
	}
	return true;
}

bool isNotEmpty(RingBufferPointer ringBuffer) {
	return !isEmpty(ringBuffer);
}

uint16_t getSize(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		uint16_t bufferSize = ringBuffer->maxSize;

		if (!ringBuffer->isFull) {
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				if (ringBuffer->head >= ringBuffer->tail) {
					bufferSize = (ringBuffer->head) - (ringBuffer->tail);
				} else {
					bufferSize = (ringBuffer->maxSize + ringBuffer->head - ringBuffer->tail);
				}
			}
		}
		return bufferSize;
	}
	return 0;
}

void addByte(RingBufferPointer ringBuffer, uint8_t byte) {
	if (ringBuffer != NULL && ringBuffer->buffer != NULL) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			ringBuffer->buffer[ringBuffer->head] = byte;
			advanceBufferPointer(ringBuffer);
		}
	}
}

bool getByte(RingBufferPointer ringBuffer, uint8_t *byte) {
	if (ringBuffer != NULL && ringBuffer->buffer != NULL) {
		if (isNotEmpty(ringBuffer)) {
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				*byte = ringBuffer->buffer[ringBuffer->tail];
				retreatBufferPointer(ringBuffer);
			}
			return true;
		}
		return false;
	}
	return false;
}

static void advanceBufferPointer(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		if (ringBuffer->isFull) {
			ringBuffer->tail = (ringBuffer->tail + 1) % ringBuffer->maxSize;
		}
		ringBuffer->head = (ringBuffer->head + 1) % ringBuffer->maxSize;
		ringBuffer->isFull = (ringBuffer->head == ringBuffer->tail);
	}
}

static void retreatBufferPointer(RingBufferPointer ringBuffer) {
	if (ringBuffer != NULL) {
		ringBuffer->isFull = false;
			ringBuffer->tail = (ringBuffer->tail + 1) % ringBuffer->maxSize;
	}
}