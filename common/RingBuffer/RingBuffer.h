#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <util/atomic.h>

/*
   Usage example:

    RingBufferPointer TxBuffer = getRingBufferInstance(BUFFER_SIZE);

    addByte(TxBuffer, 1);
    addByte(TxBuffer, 2);
    addByte(TxBuffer, 3);
    addByte(TxBuffer, 4);
    printf("Size: %d\n\n", getSize(TxBuffer));  // 4

    uint8_t value;
    while (isNotEmpty(TxBuffer)) {
        if (getByte(TxBuffer, &value)) {
            printf("Value: %d\n", value);
            printf("Size: %d\n", getSize(TxBuffer));
        }
    }
 */

typedef struct RingBuffer *RingBufferPointer;


RingBufferPointer getRingBufferInstance(uint16_t bufferSize);   // get initialized buffer, provide buffer capacity size

void reset(RingBufferPointer ringBuffer);                       // set pointers to zero, and the byte count, causing the buffer to be "empty"
void delete(RingBufferPointer ringBuffer);                      // destroying the container

bool isFull(RingBufferPointer ringBuffer);                      // check that buffer is full
bool isNotFull(RingBufferPointer ringBuffer);

bool isEmpty(RingBufferPointer ringBuffer);                     // buffer empty if no bytes received
bool isNotEmpty(RingBufferPointer ringBuffer);

uint16_t getSize(RingBufferPointer ringBuffer);                 // return byte count in buffer

void addByte(RingBufferPointer ringBuffer, uint8_t byte);       // add byte to buffer
bool getByte(RingBufferPointer ringBuffer, uint8_t *byte);      // get byte from buffer, return true if byte successfully received and false otherwise