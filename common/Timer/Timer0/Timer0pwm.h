#pragma once

#include "TimerCommon.h"
#include <stdint.h>
#include <stdbool.h>

void initTimer0PWM(uint8_t prescaler, _Bool isInverted);

void setTimer0PWMDuty(uint16_t value);
