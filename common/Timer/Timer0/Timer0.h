#pragma once

#include "TimerCommon.h"
#include <stdint.h>


void initTimer0(uint8_t prescaler, uint8_t initialValue);

uint8_t getTimerValue0();
void setTimer0(uint8_t value);
void resetTimer0();

void enableInterruptTimer0();
void disableInterruptTimer0();