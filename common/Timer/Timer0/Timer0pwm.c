#include "Timer0pwm.h"


void initTimer0PWM(uint8_t prescaler, _Bool isInverted) {
	DDRB |= (1 << PB3);					//set the proper pin as output
	TCCR0 |= (1 << COM01);
	
	if (isInverted) {					//if inversion requested
		TCCR0 |= (1 << COM00);			//set the extra bits required for inverted output
	}
	
	TCCR0 |= (1 << WGM00) | (1 << WGM01) | (prescaler & 0x07); //set the other mode bits and the prescaler (masks unused bits)
}

void setTimer0PWMDuty(uint16_t duty) {
	OCR0 = duty;
}