#pragma once

#include "TimerCommon.h"
#include <stdint.h>
#include <stdbool.h>

void initTimer1PWM(uint8_t prescaler, uint16_t top, _Bool isInverted);

void setTimer1PWM1A(uint16_t value);

void setTimer1PWM1B(uint16_t value);