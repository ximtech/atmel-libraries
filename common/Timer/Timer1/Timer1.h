#pragma once

#include "TimerCommon.h"
#include <inttypes.h>


void initTimer1(uint8_t prescaler, uint16_t initialValue);

uint8_t getTimerValue1();
void setTimer1(uint16_t value);
void resetTimer1();

void enableInterruptTimer1();
void disableInterruptTimer1();