#include "Timer1pwm.h"

void initTimer1PWM(uint8_t prescaler, uint16_t top, _Bool isInverted) {
	DDRD |= (1 << DDD4) | (1 << DDD5);					//set the proper pins as outputs
	ICR1 = top;												//set the top value of the timer
	TCCR1A = (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11); //set up the timer for standard mode
	
	if (isInverted) {									//if inversion requested
		TCCR1A |= (1 << COM1A0) | (1 << COM1B0);		//set the extra bits required for inverted output
	}
	
	TCCR1B = (1 << WGM13) | (1 << WGM12) | (prescaler & 0x07); //set the other mode bits and the prescaler (masks unused bits)
}

void setTimer1PWM1A(uint16_t value) {
	OCR1A = value;
}

void setTimer1PWM1B(uint16_t value) {
	OCR1B = value;
}