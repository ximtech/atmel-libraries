#include "Timer1.h"
#include <avr/interrupt.h>


void initTimer1(uint8_t prescaler, uint16_t initialValue) {
	if (prescaler > 5) {				//if an external clock source is used, set T2 pin as input
		DDRB &= ~(1 << PORTB0);
	}
	resetTimer1();
	setTimer1(initialValue);
	TCCR1A &= ~((1 << WGM11) | (1 << WGM10));	// set normal mode
	TCCR1B = prescaler & 0x07;        //set the prescaler and mask bits that aren't the prescaler
}

uint8_t getTimerValue1() {
	return TCNT1;                     //return the timer counter 0 register value
}

void setTimer1(uint16_t value) {
	TCNT1 = value;                       //set the timer0 register
}

void resetTimer1() {
	TCNT1 = 0;                       //set the timer0 register to 0, clearing the timer
	TIFR = 0x1;
}

void enableInterruptTimer1() {
	TIMSK |= (1 << TOIE1);            //set the timer overflow interrupt enable 0 bit
	TIFR |= (1 << TOV1);
	sei();                            //set the global interrupt flag
}

void disableInterruptTimer1() {
	TIMSK &= ~(1 << TOIE1);           //clear the timer overflow interrupt enable 0 bit
}
