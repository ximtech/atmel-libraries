#pragma once

#include <avr/io.h>

// Timer 0 prescalers
#define TIMER0_PRESCALER_1    (1 << CS00)
#define TIMER0_PRESCALER_8    (1 << CS01)
#define TIMER0_PRESCALER_64   (1 << CS01) | (1 << CS00)
#define TIMER0_PRESCALER_256  (1 << CS02)
#define TIMER0_PRESCALER_1024 (1 << CS02) | (1 << CS00)

#define TIMER0_PRESCALER_EXTERNAL_RISING_EDGE  (1 << CS02) | (1 << CS01)					//external clock, clock on rising edge of t0 pin
#define TIMER0_PRESCALER_EXTERNAL_FALLING_EDGE (1 << CS02) | (1 << CS01) | (1 << CS00)	//external clock, clock on falling edge of t0 pin

// Timer 1 prescalers
#define TIMER1_PRESCALER_1    (1 << CS10)
#define TIMER1_PRESCALER_8    (1 << CS11)
#define TIMER1_PRESCALER_64   (1 << CS11) | (1 << CS10)
#define TIMER1_PRESCALER_256  (1 << CS12)
#define TIMER1_PRESCALER_1024 (1 << CS12) | (1 << CS10)

#define TIMER1_PRESCALER_EXTERNAL_RISING  (1 << CS12) | (1 << CS11)				  //external clock, clock on rising edge of t1 pin
#define TIMER1_PRESCALER_EXTERNAL_FALLING (1 << CS12) | (1 << CS11) | (1 << CS10) //external clock, clock on falling edge of t1 pin

// PWM types for Timer 1
#define TIMER1_PWM_8BIT  255
#define TIMER1_PWM_9BIT  511
#define TIMER1_PWM_10BIT 1023
#define TIMER1_PWM_11BIT 2047
#define TIMER1_PWM_12BIT 4095

// Prescalers for Timer 2
#define TIMER2_PRESCALER_1    (1 << CS20)
#define TIMER2_PRESCALER_8    (1 << CS21)
#define TIMER2_PRESCALER_32   (1 << CS21) | (1 << CS20)
#define TIMER2_PRESCALER_64   (1 << CS22)
#define TIMER2_PRESCALER_128  (1 << CS22) | (1 << CS20)
#define TIMER2_PRESCALER_256  (1 << CS22) | (1 << CS21)
#define TIMER2_PRESCALER_1024 (1 << CS22) | (1 << CS21) | (1 << CS20)

// Timer 2 clock sources
#define TIMER2_SOURCE_INTERNAL 0
#define TIMER2_SOURCE_EXTERNAL 1

// Pin functions for CTC
#define PIN_DISCONNECT 0                              //disconnect pin from timer
#define PIN_TOGGLE	   1                              //toggle pin on compare match
#define PIN_CLEAR      2                              //clear (set to 0) pin on compare match
#define PIN_SET        3                              //set (set to 1) pin on compare match