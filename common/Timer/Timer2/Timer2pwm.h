#pragma once

#include "TimerCommon.h"
#include <stdint.h>
#include <stdbool.h>

void initTimer2PWM(uint8_t prescaler, _Bool isInverted);

void setTimer2PWMDuty(uint16_t value);