#include "Timer2pwm.h"


void initTimer2PWM(uint8_t prescaler, _Bool isInverted) {
	DDRB |= (1 << PB3);					//set the proper pin as output
	TCCR2 |= (1 << COM21);
	
	if (isInverted) {					//if inversion requested
		TCCR2 |= (1 << COM20);			//set the extra bits required for inverted output
	}
	
	TCCR2 |= (1 << WGM20) | (1 << WGM21) | (prescaler & 0x07); //set the other mode bits and the prescaler (masks unused bits)
}

void setTimer2PWMDuty(uint16_t duty) {
	OCR2 = duty;
}
