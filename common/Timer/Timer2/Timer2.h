#pragma once

#include "TimerCommon.h"
#include <stdint.h>


void initTimer2(uint8_t prescaler, uint8_t initialValue);

uint8_t getTimerValue();
void setTimer2(uint8_t value);
void resetTimer2();

void enableInterruptTimer2();
void disableInterruptTimer2();
