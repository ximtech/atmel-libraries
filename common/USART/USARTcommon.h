#pragma once

#include <avr/io.h>

// Commonly used baud rates
#define BAUD_RATE_1200   1200
#define BAUD_RATE_2400   2400
#define BAUD_RATE_4800   4800
#define BAUD_RATE_9600   9600
#define BAUD_RATE_19200  19200
#define BAUD_RATE_115200 115200

// Data bits in a frame for Receiver and Transmitter
#define DATA_BITS_9		(UCSZ2 << 1) | (UCSZ1 << 1) | (1 << UCSZ0)
#define DATA_BITS_8		(UCSZ1 << 1) | (1 << UCSZ0)
#define DATA_BITS_7		(1 << UCSZ1)
#define DATA_BITS_6		(1 << UCSZ0)
#define DATA_BITS_5		0

// Bits for set type of parity generation check
#define PARITY_MODE_DISABLED		0
#define PARITY_MODE_ENABLED_EVEN	(1 << UPM1)
#define PARITY_MODE_ENABLED_ODD		(1 << UPM1) | (1 << UPM0)

// Number of Stop Bits to be inserted by the Transmitter
#define STOP_BIT_1	0
#define STOP_BIT_2  (1 << USBS)