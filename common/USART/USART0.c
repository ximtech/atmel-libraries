#include "USART0.h"

void initUsart0(uint8_t options) {
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	#if USE_2X
	UCSRA |= (1 << U2X);
	#else
	UCSRA &= ~(1 << U2X);
	#endif
	UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);	// enable USART with interrupt
	UCSRC = (1 << URSEL) | options;						// set your mode(char size)
}

uint8_t readByteUsart0() {
	loop_until_bit_is_set(UCSRA, RXC);      // wait till data is received
	return UDR;                             // return the byte
}

void sendByteUart0(uint8_t byteToSend) {
	loop_until_bit_is_set(UCSRA, UDRE);		// wait for empty transmit buffer
	UDR = byteToSend;	                    // send the byte
}

void sendStringUsart0(char *string) {
	uint8_t i = 0;
	while (string[i] != 0) {
		sendByteUart0(string[i]);
		i++;
	}
}

void readStringUart0(char *charArray, uint16_t length) {
	for (uint16_t i = 0; i < length; i++) {		// get the byte and put it in array
		charArray[i] = readByteUsart0();
	}
}

void readStringUntilStopCharUsart0(char *charArray, uint16_t length, char stopchar) {  //get n bytes or until stopchar is received
	for (uint16_t i = 0; i < length; i++) {      // for n bytes
		char receivedChar = readByteUsart0();    // get the next byte
		if(receivedChar == stopchar) {			 // if the next byte is the stopping character, quit the loop
			break;
		}
		charArray[i] = receivedChar;
	}
}
