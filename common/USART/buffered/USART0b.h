#pragma once

//#define F_CPU 8000000UL			// define frequency here, by default its 8MHz
//#define BAUD  BAUD_RATE_9600

#include "USARTcommon.h"
#include "RingBuffer.h"

#include <util/setbaud.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#ifndef RX_BUFFER_SIZE0
#warning RECEIVE_BUFFER_SIZE0 not defined, setting to 128
#define RX_BUFFER_SIZE0 128
#endif

#ifndef TX_BUFFER_SIZE0
#warning TRANSMIT_BUFFER_SIZE0 not defined, setting to 128
#define TX_BUFFER_SIZE0 128
#endif

void initBufferedUsart0(uint8_t options);	// pass usart options, see USARTcommon.h

void sendByteUsart0(uint8_t byte);
void sendStringUsart0(char *string);
void sendStringlnUsart0(char *string);
void sendStringUsart0_P(const char *string);
void sendStringlnUsart0_P(const char *string);

uint8_t readByteUsart0();
void readStringUart0(char *charArray);
void readStringForLengthUart0(char *charArray, uint16_t length);
void readStringUntilStopCharUsart0(char *charArray, char stopchar);

bool isRxBufferEmpty0();
bool isRxBufferNotEmpty0();

void resetRxBufferUsart0();
void resetTxBufferUsart0();
void deleteUsart0();