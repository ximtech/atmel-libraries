#pragma once

//#define F_CPU 8000000UL			// define frequency here, by default its 8MHz
//#define BAUD  BAUD_RATE_9600

#include "USARTcommon.h"
#include "USART0.h"
#include <util/setbaud.h>

void initUsart0(uint8_t options);

uint8_t readByteUsart0();
void sendByteUart0(uint8_t byteToSend);

void sendStringUsart0(char *string);
void readStringUart0(char *charArray, uint16_t length);
void readStringUntilStopCharUsart0 (char *charArray, uint16_t length, char stopchar);