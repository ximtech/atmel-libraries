#pragma once

#include "libs/USART0b.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <util/delay.h>

#define DEFAULT_TIMEOUT				  10000
#define KEEPALIVE_ATTEMPT_COUNT		  5
#define COMMAND_MAX_LENGTH			  60
#define SSID_LENGTH					  20

typedef enum ESP8266ResponseStatus {
	ESP8266_RESPONSE_SUCCESS,
	ESP8266_RESPONSE_ERROR,
	ESP8266_RESPONSE_TIMEOUT,
	ESP8266_RESPONSE_BUFFER_FULL
} ResponseStatus;

typedef enum ESP8266ConnectionStatus {
	ESP8266_CONNECTED_TO_AP,
	ESP8266_CREATED_TRANSMISSION,
	ESP8266_TRANSMISSION_DISCONNECTED,
	ESP8266_NOT_CONNECTED_TO_AP,
	ESP8266_CONNECT_UNKNOWN_ERROR
} ConnectionStatus;

typedef enum ESP8266AccessPointConnectionStatus {
	ESP8266_WIFI_CONNECTED,
	ESP8266_CONNECTION_TIMEOUT,
	ESP8266_WRONG_PASSWORD,
	ESP8266_NOT_FOUND_TARGET_AP,
	ESP8266_CONNECTION_FAILED,
	ESP8266_JOIN_UNKNOWN_ERROR
} APConnectionStatus;

typedef enum ESP8266WiFiMode {
	ESP8266_STATION				   = 1,
	ESP8266_ACCESSPOINT			   = 2,
	ESP8266_STATION_AND_ACCESPOINT = 3
} WiFiMode;

typedef enum ESP8266ConnectionMode {
	ESP8266_SINGLE   = 0,
	ESP8266_MULTIPLE = 1 
} ConnectionMode;

typedef enum ESP8266TransferMode {
	ESP8266_NORMAL      = 0,
	ESP8266_TRANSPARENT = 1
} TransferMode;

typedef enum ESP8266ConnectionID {
	CONNECTION_ID_1 = 1,
	CONNECTION_ID_2 = 2,
	CONNECTION_ID_3 = 3,
	CONNECTION_ID_4 = 4,
} ConnectionID;

typedef enum WifiEncryptionType {
	OPEN,
	WEP,
	WPA_PSK,
	WPA2_PSK,
	WPA_WPA2_PSK,
	WPA2_ENTERPRISE // AT can NOT connect to WPA2_Enterprise AP for now
} WifiEncryptionType;

typedef enum SignalStrength {
	RSSI_30dBm = 30,     // Maximum signal strength
	RSSI_50dBm = 50,     // Excellent signal strength
	RSSI_60dBm = 60,     // Good, reliable signal strength
	RSSI_67dBm = 67,     // Reliable signal strength. The minimum for any service depending on a reliable connection
	RSSI_70dBm = 70,     // Not a strong signal
	RSSI_80dBm = 80,     // Unreliable signal strength, will not suffice for most services
	RSSI_90dBm = 90      // The chances of even connecting are very low at this level
} SignalStrength;


typedef struct ResponseData {
	ResponseStatus responseStatus;
	char *responseBody;
} ResponseData;

typedef struct AccessPoint {
	WifiEncryptionType encryption;
	char ssid[SSID_LENGTH];
	int8_t signalStrength;
} AccessPoint;


APConnectionStatus begin(char *ssid, char *password);

ResponseStatus initWifi();
ResponseData *moduleHealthCheck();
ResponseData *restartWifi();
ResponseData *setWifiMode(WiFiMode wifiMod);
ResponseData *setConnectionMode(ConnectionMode connectionMode);
ResponseData *setApplicationMode(TransferMode transferMode);
ResponseData *enableDeepSleepMode(uint16_t timeToSleepMs);
ResponseData *requestAvailableAccessPoints();

ConnectionStatus getConnectionStatus();
APConnectionStatus connectToAccessPoint(char *ssid, char *password);
ResponseData *disconnectFromAccessPoint();

ResponseData *connect(char *host, char *port);
ResponseData *multipleConnect(ConnectionID id, char *host, char *port);
ResponseData *closeConnection();
ResponseData *closeMultipleConnection(ConnectionID id);

ResponseData *send(char *data);
ResponseData *multipleSend(ConnectionID id, char *data);

void getLocalIP(char *buffer);
uint8_t receivedAccessPointCount();
void parseAvailableAccessPoints(AccessPoint *APList, size_t length, SignalStrength minSignalStrength);


typedef struct WiFi {
	APConnectionStatus (*begin)(char *ssid, char *password);		//default module initialization and access to AP
	
	ResponseStatus (*initWifi)();
	ResponseData *(*moduleHealthCheck)();
	ResponseData *(*restart)();					// software restart
	ResponseData *(*setWifiMode)(WiFiMode mode);
	ResponseData *(*setConnectionMode)(ConnectionMode mode);
	ResponseData *(*setApplicationMode)(TransferMode mode);
	ResponseData *(*deepSleepMode)(uint16_t timeToSleepMs);
	ResponseData *(*requestAvailableAccessPoints)();
	
	ConnectionStatus (*getConnectionStatus)();
	APConnectionStatus (*connectToAccessPoint)(char *ssid, char *password);
	ResponseData *(*disconnectFromAccessPoint)();
	
	ResponseData *(*connect)(char *host, char *port);
	ResponseData *(*multipleConnect)(ConnectionID id, char *host, char *port);
	ResponseData *(*closeConnection)();
	ResponseData *(*closeMultipleConnection)(ConnectionID id);
	
	ResponseData *(*send)(char *data);	// send request data to host with parameters. Example: send("GET /update?api_key=API_WRITE_KEY&field1=Sample")
	ResponseData *(*multipleSend)(ConnectionID id, char *data);	// same as send, but with multiple host connection
	
	void (*getLocalIP)(char *buffer);
	uint8_t (*receivedAccessPointCount)();
	void (*parseAvailableAccessPoints)(AccessPoint *APList, size_t length, SignalStrength minSignalStrength);
} WiFi;


WiFi *getWiFiInstance();
void deleteWifi(WiFi *instance);


// Helper functions
inline bool isResponseStatusSuccess(ResponseData *response) {
	return (response->responseStatus == ESP8266_RESPONSE_SUCCESS);
}

inline bool isResponseStatusBufferFull(ResponseData *response) {
	return (response->responseStatus == ESP8266_RESPONSE_BUFFER_FULL);
}

inline bool isResponseStatusError(ResponseData *response) {
	return (response->responseStatus == ESP8266_RESPONSE_ERROR);
}

inline bool substring(const char *start, const char *end, const char *source, char *dest) {
	char *startPointer = strstr(source, start);
	if (startPointer != NULL) {  // check that substring start is found
		startPointer += strlen(start);
		size_t substringLength = strcspn(startPointer, end);
		if (substringLength > 0) {  // check that substring end is found
			strncat(dest, startPointer, substringLength);
			return true;
		}
		return false;
	}
	return false;
}