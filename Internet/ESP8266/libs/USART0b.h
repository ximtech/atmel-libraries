#pragma once

//#define F_CPU 12000000UL			// define frequency here, by default its 12MHz for working with module baud rate 115200
#define BAUD  BAUD_RATE_115200

#include "USARTcommon.h"
#include "RingBuffer.h"

#include <util/setbaud.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//#define RX_BUFFER_SIZE0 1000	// buffer for receiving data from module

#ifndef RX_BUFFER_SIZE0
#warning RECEIVE_BUFFER_SIZE0 not defined, setting to 160
#define RX_BUFFER_SIZE0 160
#endif

void initBufferedUsart0(uint8_t options);	// pass usart options, see USARTcommon.h

void sendByteUsart0(uint8_t byte);
void sendStringUsart0(char *string);
void sendStringlnUsart0(char *string);
void sendStringUsart0_P(const char *string);
void sendStringlnUsart0_P(const char *string);

uint8_t readByteUsart0();
void readStringUart0(char *charArray);
void readStringForLengthUart0(char *charArray, uint16_t length);
void readStringUntilStopCharUsart0(char *charArray, char stopchar);

bool isRxBufferEmpty0();
bool isRxBufferNotEmpty0();
uint16_t getByteCountInBuffer();
char *getRxBuffer();

void resetRxBufferUsart0();
void resetTxBufferUsart0();
void deleteUsart0();