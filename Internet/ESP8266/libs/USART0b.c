#include "USART0b.h"

static volatile RingBufferPointer RxBuffer;

ISR (USART_RXC_vect) {         // received a byte ISR
	if (isNotFull(RxBuffer)) {		// when buffer overflow, doesn't overwrite non read data
		uint8_t byte = UDR;
		addByte(RxBuffer, byte);
	}
}

void initBufferedUsart0(uint8_t options) {
	RxBuffer = getRingBufferInstance(RX_BUFFER_SIZE0);
	
	if (RxBuffer != NULL) {
		UBRRH = UBRRH_VALUE;
		UBRRL = UBRRL_VALUE;
		#if USE_2X
		UCSRA |= (1 << U2X);
		#else
		UCSRA &= ~(1 << U2X);
		#endif
		UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);	// enable USART with interrupt
		UCSRC = (1 << URSEL) | options;						// set your mode(char size)
		sei();												// turn on global interrupts
	}			
}

void sendByteUsart0(uint8_t byte) {
	UDR = byte;
	while (!(UCSRA & (1<<UDRE)));
}

void sendStringUsart0(char *string) {
	uint8_t byte = 0;
	uint16_t i = 0;
	while ((byte = string[i]) != '\0') {
		sendByteUsart0(byte);
		i++;
	}
}

void sendStringlnUsart0(char *string) {
	sendStringUsart0(string);
	sendStringUsart0("\r\n");
}

void sendStringUsart0_P(const char *string) {
	uint8_t byte = 0;
	uint16_t i = 0;
	while ((byte = pgm_read_byte(&(string[i]))) != '\0') {
		sendByteUsart0(byte);
		i++;
	}
}

void sendStringlnUsart0_P(const char *string) {
	sendStringUsart0_P(string);
	sendStringUsart0_P(PSTR("\r\n"));
}

uint8_t readByteUsart0() {
	uint8_t byte = 0;
	getByte(RxBuffer, &byte);
	return byte;
}

void readStringUart0(char *charArray) {
	uint16_t i = 0;
	uint8_t byte = 0;
	while(getByte(RxBuffer, &byte)) {
		charArray[i] = byte;
		i++;
	}
}

void readStringForLengthUart0(char *charArray, uint16_t length) {
	uint16_t i = 0;
	uint8_t byte = 0;
	while(getByte(RxBuffer, &byte) && i < length) {
		charArray[i] = byte;
		i++;
	}
}

void readStringUntilStopCharUsart0(char *charArray, char stopchar) {  //get bytes or until stopchar is received
	uint16_t length = 0;
	while (!isRxBufferEmpty0()) {
		char receivedChar = readByteUsart0();    // get the next byte
		if (receivedChar == stopchar) {			 // if the next byte is the stopping character, quit the loop
			break;
		}
		charArray[length] = receivedChar;
		length++;
	}
}

bool isRxBufferEmpty0() {
	return isEmpty(RxBuffer);    // buffer empty if no bytes received
}

bool isRxBufferNotEmpty0() {
	return isNotEmpty(RxBuffer);
}

uint16_t getByteCountInBuffer() {
	return getSize(RxBuffer);
}

char *getRxBuffer() {
	return (char *)getBufferPointer(RxBuffer);
}

void resetRxBufferUsart0() {
	reset(RxBuffer);
}

void deleteUsart0() {
	delete(RxBuffer);
	UCSRB = 0;
	UCSRC = 0;
}