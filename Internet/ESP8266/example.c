#include "ESP8266_WiFi.h" 


int main() {
	
	
	WiFi wifi = *getWiFiInstance();
	
	APConnectionStatus statusOther = wifi.begin("BALTICOM2G46", "adzdm99qmjwg");	// connect to existing AP
	if (statusOther == ESP8266_WIFI_CONNECTED) {
		
		ResponseData *response = wifi.requestAvailableAccessPoints(); // send AP discovery command
		if (isResponseStatusSuccess(response) || isResponseBufferFull(response)) {	// received data can be more than buffer size, but data still can be parsed
			uint8_t count = wifi.receivedAccessPointCount();
			
			AccessPoint APList[count];
			wifi.parseAvailableAccessPoints(APList, count, RSSI_90dBm);	// get all available AP, with low signal included
			
			char buffer[20];
			for (uint8_t i = 0; i < count; i++) {	// Output example: TP-Link_5F1D : -59
				sendStringUsart0(APList[i].ssid);
				sendStringUsart0(" : ");
				itoa(APList[i].signalStrength, buffer, 10);
				sendStringUsart0(buffer);
				memset(buffer, 0, 20);
				sendStringUsart0("\n");
			}
			
			// IP
			memset(buffer, 0, 20);
			wifi.getLocalIP(buffer);
			sendStringlnUsart0(buffer);
			
			// Send demo
			_delay_ms(1000);
			response = wifi.connect("api.thingspeak.com", "80");	// connect to server
			response = wifi.send("GET /update?api_key=4VHW19QJ6LFR7RA1&field1=21");
			if (isResponseStatusSuccess(response)) {
				sendStringlnUsart0("Send OK!\r\n");
			}
			
			// Receive demo
			_delay_ms(15000);	// Thingspeak server delay
			response = wifi.connect("api.thingspeak.com", "80");
			response = wifi.send("GET /channels/1243676/feeds/last.txt");
			if (isResponseStatusSuccess(response)) {
				sendStringlnUsart0(response->responseBody);
				sendStringlnUsart0("Receive OK!");
			}
			
		}
		
	}
	
	deleteWifi(&wifi);
	
	
	
	while (true) {
	}
		
	
}

