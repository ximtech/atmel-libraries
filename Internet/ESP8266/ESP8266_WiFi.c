#include "ESP8266_WiFi.h"

#define OK_STATUS		"\r\nOK"
#define CLOSED_STATUS	"CLOSED"
#define ERROR_STATUS	"ERROR"
#define FAIL_STATUS		"FAIL"

#define RECEIVE_DATA_WAIT_MS	100

static enum AccessPointParameter {
	SECURITY, SSID, SIGNAL_STRENGTH
} APParameter;

static ResponseData *sendParametricATCommand(const char *ATCommandPattern, ...);
static ResponseData *sendATCommand(const char *ATCommand);
static ResponseData *readResponse();
static bool isResponseOK(char *responseBody);
static bool isResponseError(char *responseBody);
static bool isRxBufferFull(char *responseBody);
static void initListOfAccessPoints(AccessPoint *APList, size_t length);
static void clearAccessPointData(AccessPoint *AP);
static void parseToAP(AccessPoint *AP, char *buffer);
static void parseAPParamString(char *stringToParse, char *result);

static ResponseData *response;

WiFi * getWiFiInstance() {
	WiFi *instance = malloc(sizeof(WiFi));
	
	if (instance != NULL) {
		instance->begin = begin;
		
		instance->initWifi = initWifi;
		instance->moduleHealthCheck = moduleHealthCheck;
		instance->restart = restartWifi;
		instance->setWifiMode = setWifiMode;
		instance->setConnectionMode = setConnectionMode;
		instance->setApplicationMode = setApplicationMode;
		instance->deepSleepMode = enableDeepSleepMode;
		instance->requestAvailableAccessPoints = requestAvailableAccessPoints;
		
		instance->getConnectionStatus = getConnectionStatus;
		instance->connectToAccessPoint = connectToAccessPoint;
		instance->disconnectFromAccessPoint = disconnectFromAccessPoint;
		
		instance->connect = connect;
		instance->multipleConnect = multipleConnect;
		instance->closeConnection = closeConnection;
		instance->closeMultipleConnection = closeMultipleConnection;
		
		instance->send = send;
		instance->multipleSend = multipleSend;
		
		instance->getLocalIP = getLocalIP;
		instance->receivedAccessPointCount = receivedAccessPointCount;
		instance->parseAvailableAccessPoints = parseAvailableAccessPoints;
	}
	return instance;
}

APConnectionStatus begin(char *ssid, char *password) {
	ResponseStatus status = initWifi();
	
	if (status == ESP8266_RESPONSE_SUCCESS) {
		ResponseData *response = setWifiMode(ESP8266_STATION_AND_ACCESPOINT);
		status = response->responseStatus;
		
		if (status == ESP8266_RESPONSE_SUCCESS) {
			response = setConnectionMode(ESP8266_SINGLE);
			status = response->responseStatus;
		}
		
		if (status == ESP8266_RESPONSE_SUCCESS) {
			response = setApplicationMode(ESP8266_NORMAL);
			status = response->responseStatus;
		}
		
		if (status == ESP8266_RESPONSE_SUCCESS) {
			ConnectionStatus connectionStatus = getConnectionStatus();
			if (connectionStatus == ESP8266_NOT_CONNECTED_TO_AP || 
				connectionStatus == ESP8266_TRANSMISSION_DISCONNECTED) {
				return connectToAccessPoint(ssid, password);
			} else if (connectionStatus == ESP8266_CONNECTED_TO_AP) {
				return ESP8266_WIFI_CONNECTED;
			}
		}
	}
	
	if (status == ESP8266_RESPONSE_TIMEOUT) {
		return ESP8266_CONNECTION_TIMEOUT;
	}
	return ESP8266_CONNECTION_FAILED;
}

ResponseStatus initWifi() {
	initBufferedUsart0(DATA_BITS_8 | PARITY_MODE_DISABLED | STOP_BIT_1);
	_delay_ms(100);	// initial delay
	response = malloc(sizeof(ResponseData));
	
	if (response != NULL) {
		response->responseBody = getRxBuffer();
		ResponseData *response = moduleHealthCheck();
		if (isResponseStatusSuccess(response)) {
			response = sendATCommand(PSTR("ATE0"));
			return response->responseStatus;
		}
		return response->responseStatus;	
	}
	return ESP8266_RESPONSE_ERROR;
}

ResponseData *moduleHealthCheck() {
	return sendATCommand(PSTR("AT"));
}

ResponseData *restartWifi() {
	return sendATCommand(PSTR("AT+RST"));
}

ResponseData *setWifiMode(WiFiMode wifiMod) {
	return sendParametricATCommand(PSTR("AT+CWMODE=%d"), wifiMod);
}

ResponseData *setConnectionMode(ConnectionMode connectionMode) {
	return sendParametricATCommand(PSTR("AT+CIPMUX=%d"), connectionMode);
}

ResponseData *setApplicationMode(TransferMode transferMode) {
	return sendParametricATCommand(PSTR("AT+CIPMODE=%d"), transferMode);
}

ResponseData *enableDeepSleepMode(uint16_t timeToSleepMs) {	// Hardware has to support deep-sleep wake up (Reset pin has to be High).
	return sendParametricATCommand(PSTR("AT+GSLP=%d"), timeToSleepMs);
}

ResponseData *requestAvailableAccessPoints() {
	ResponseData *response = sendATCommand(PSTR("AT+CWLAP"));
	_delay_ms(RECEIVE_DATA_WAIT_MS);
	return response;
}

ConnectionStatus getConnectionStatus() {
	ResponseData *response = sendATCommand(PSTR("AT+CIPSTATUS"));
	
	if (response->responseStatus == ESP8266_RESPONSE_SUCCESS) {
		if (strstr_P(response->responseBody, PSTR("STATUS:2"))) {
			return ESP8266_CONNECTED_TO_AP;
		} else if (strstr_P(response->responseBody, PSTR("STATUS:3"))) {
			return ESP8266_CREATED_TRANSMISSION;
		} else if (strstr_P(response->responseBody, PSTR("STATUS:4"))) {
			return ESP8266_TRANSMISSION_DISCONNECTED;
		} else if (strstr_P(response->responseBody, PSTR("STATUS:5"))) {
			return ESP8266_NOT_CONNECTED_TO_AP;
		}
	}
	return ESP8266_CONNECT_UNKNOWN_ERROR;
}

APConnectionStatus connectToAccessPoint(char *ssid, char *password) {
	ResponseData *response = sendParametricATCommand(PSTR("AT+CWJAP=\"%s\",\"%s\""), ssid, password);	// connect to access point and save connection credentials
	
	if (isResponseStatusSuccess(response)) {
		return ESP8266_WIFI_CONNECTED;
	} else if (response->responseStatus == ESP8266_RESPONSE_ERROR) {
		if(strstr_P(response->responseBody, PSTR("+CWJAP:1"))) {
			return ESP8266_CONNECTION_TIMEOUT;
		} else if(strstr_P(response->responseBody, PSTR("+CWJAP:2"))) {
			return ESP8266_WRONG_PASSWORD;
		} else if(strstr_P(response->responseBody, PSTR("+CWJAP:3"))) {
			return ESP8266_NOT_FOUND_TARGET_AP;
		} else if(strstr_P(response->responseBody, PSTR("+CWJAP:4"))) {
			return ESP8266_CONNECTION_FAILED;
		}
	}
	return ESP8266_JOIN_UNKNOWN_ERROR;
}

ResponseData *disconnectFromAccessPoint() {
	return sendATCommand(PSTR("AT+CWQAP"));
}

ResponseData *connect(char *host, char *port) {
	ResponseData *response = sendParametricATCommand(PSTR("AT+CIPSTART=\"TCP\",\"%s\",%s"), host, port);
	if (isResponseStatusError(response)) {
		if (strstr_P(response->responseBody, PSTR("ALREADY CONNECTED"))) {
			response->responseStatus = ESP8266_RESPONSE_SUCCESS;
		}
	}
	return response;
}

ResponseData *multipleConnect(ConnectionID id, char *host, char *port) {
	return sendParametricATCommand(PSTR("AT+CIPSTART=\"%d\",\"TCP\",\"%s\",%s"), id, host, port);
}

ResponseData *send(char *data) {
	ResponseData *response = sendParametricATCommand(PSTR("AT+CIPSEND=%d"), (strlen(data) + 2));	// provide data length before request
	if (isResponseStatusSuccess(response)) {
		resetRxBufferUsart0();
		sendStringlnUsart0(data);
		_delay_ms(RECEIVE_DATA_WAIT_MS);
		return readResponse();
	}
	return response;
}

ResponseData *multipleSend(ConnectionID id, char *data) {
	ResponseData *response = sendParametricATCommand(PSTR("AT+CIPSEND=%d,%d"), id, (strlen(data) + 2));	// provide connection id and data length before request
	if (isResponseStatusSuccess(response)) {
		resetRxBufferUsart0();
		sendStringlnUsart0(data);
		_delay_ms(RECEIVE_DATA_WAIT_MS);
		return readResponse();
	}
	return response;
}

ResponseData *closeConnection() {
	return sendATCommand(PSTR("AT+CIPCLOSE"));
}

ResponseData *closeMultipleConnection(ConnectionID id) {	//  ID no. of connection to close, when id=5, all connections will be closed.
	return sendParametricATCommand(PSTR("AT+CIPCLOSE=%d"), id);
}

void getLocalIP(char *buffer) {
	ResponseData *response = sendATCommand(PSTR("AT+CIFSR"));
	if (isResponseStatusSuccess(response)) {
		substring("STAIP,\"", "\"", response->responseBody, buffer);
	}
}

uint8_t receivedAccessPointCount() {
	uint8_t count = 0;
	const char *tmp = response->responseBody;
	while ((tmp = strstr_P(tmp, PSTR("+CWLAP:")))) {
		count++;
		tmp++;
	}
	return count;
}

void parseAvailableAccessPoints(AccessPoint *APList, size_t length, SignalStrength minSignalStrength) {
	initListOfAccessPoints(APList, length);
	uint8_t tmpBufferLength = 100;
	char buffer[tmpBufferLength];
	char *token;
	char *source = response->responseBody;
	uint8_t i = 0;

	while ((token = strtok_r(source, "\n", &source)) && i < length) {
		memset(buffer, 0, tmpBufferLength);

		if (substring("+CWLAP:(", ")", token, buffer)) {
			parseToAP(&APList[i], buffer);
			if (abs(APList[i].signalStrength) >  minSignalStrength) {
				clearAccessPointData(&APList[i]);
				continue;
			}
		}
		i++;
	}
}

void deleteWifi(WiFi *instance) {
	deleteUsart0();
	free(response);
	if (instance != NULL) {
		free(instance);
	}
}


static ResponseData *sendParametricATCommand(const char *ATCommandPattern, ...) {
	char ATCommand[COMMAND_MAX_LENGTH] = { [0 ... COMMAND_MAX_LENGTH - 1] = 0 };
	va_list valist;
	va_start(valist, ATCommandPattern);
	vsprintf_P(ATCommand, ATCommandPattern, valist);
	va_end(valist);
	
	ResponseData *response;
	for (uint8_t i = 0; i < KEEPALIVE_ATTEMPT_COUNT; i++) {
		resetRxBufferUsart0();
		sendStringlnUsart0(ATCommand);
		response = readResponse();
		if (isResponseStatusSuccess(response) || 
			isResponseStatusBufferFull(response) || 
			isResponseStatusError(response)) {
			break;
		}
	}
	return response;
}

static ResponseData *sendATCommand(const char *ATCommand) {	// send AT command to ESP8266
	ResponseData *response;
	for (uint8_t i = 0; i < KEEPALIVE_ATTEMPT_COUNT; i++) {
		resetRxBufferUsart0();
		sendStringlnUsart0_P(ATCommand);				// ESP8266 expects <CR><LF> or CarriageReturn and LineFeed at the end of each command
		response = readResponse();
		if (isResponseStatusSuccess(response) || 
			isResponseStatusBufferFull(response) || 
			isResponseStatusError(response)) {
			break;
		}
	}
	return response;
}

static ResponseData *readResponse() {
	uint32_t timeOutCounter = 0;
	uint16_t byteCountInBuffer = 0;
	response->responseStatus = ESP8266_RESPONSE_TIMEOUT;
	
	while (true) {
		
		if (timeOutCounter >= DEFAULT_TIMEOUT) {
			break;
		}
		
		byteCountInBuffer = getByteCountInBuffer();
		if (isRxBufferNotEmpty0()) {
			_delay_ms(1);		// wait some time for data
			timeOutCounter++;
			
			if (byteCountInBuffer == getByteCountInBuffer()) {	// check that no new data is arrived, no new data available
				if (isResponseOK(response->responseBody)) {
					response->responseStatus = ESP8266_RESPONSE_SUCCESS;
					break;
				} else if (isResponseError(response->responseBody)) {
					response->responseStatus = ESP8266_RESPONSE_ERROR;
					break;
				} else if (isRxBufferFull(response->responseBody)) {
					response->responseStatus = ESP8266_RESPONSE_BUFFER_FULL;
					break;
				}
			}
		}
		
		_delay_ms(1);
		timeOutCounter++;
	}
	
	return response;
}

static bool isResponseOK(char *responseBody) {	// closed status used as marker of http request success end
	return strstr_P(responseBody, PSTR(OK_STATUS)) || strstr_P(responseBody, PSTR(CLOSED_STATUS));
}

static bool isResponseError(char *responseBody) {	// find for "error" or "fail" response status
	return strstr_P(responseBody, PSTR(ERROR_STATUS)) || strstr_P(responseBody, PSTR(FAIL_STATUS));
}

static bool isRxBufferFull(char *responseBody) {
	return strlen(responseBody) >= RX_BUFFER_SIZE0;
}

static void initListOfAccessPoints(AccessPoint *APList, size_t length) {
	for (int i = 0; i < length; ++i) {
		clearAccessPointData(&APList[i]);
	}
}

static void clearAccessPointData(AccessPoint *AP) {
	AP->encryption = 0;
	memset(AP->ssid, 0, SSID_LENGTH);
	AP->signalStrength = 0;
}

static void parseToAP(AccessPoint *accessPoint, char *buffer) {
	char *source = buffer;
	char *parameterValue;
	APParameter = SECURITY;

	while ((APParameter < SIGNAL_STRENGTH + 1) && (parameterValue = strtok_r(source, ",", &source))) {
		switch (APParameter) {
			case SECURITY:
				accessPoint->encryption = atoi(parameterValue);
				break;
			case SSID:
				parseAPParamString(parameterValue, accessPoint->ssid);
				break;
			case SIGNAL_STRENGTH:
				accessPoint->signalStrength = atoi(parameterValue);
				break;
			default:
				break;
		}
		APParameter++;
	}
}

static void parseAPParamString(char *stringToParse, char *result) {
	char buffer[SSID_LENGTH] = { [0 ... SSID_LENGTH - 1] = 0 };
	substring("\"", "\"", stringToParse, buffer);
	strcpy(result, buffer);
}