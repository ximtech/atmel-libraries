#include "http/wifi/HTTPWifi.h"


int main() {
	
	WiFi wifi = *getWiFiInstance();
	
	APConnectionStatus statusOther = wifi.begin("BALTICOM2G46", "adzdm99qmjwg");	// connect to existing AP
	if (statusOther == ESP8266_WIFI_CONNECTED) {
		
		HTTP http = *getWifiHttpInstance(&wifi);
		
		HTTPResponse resp = http.GET("http://api.thingspeak.com/update")
				.bindParam("api_key", "4VHW19QJ6LFR7RA1")
				.bindParam("field1", "23")
				.addHeader(HOST, "api.thingspeak.com")
				.addHeader(CONNECTION, "close")
				.execute();
			
			sendStringlnUsart0(resp.body);
			
			_delay_ms(15000);	// Thingspeak server delay
			
		HTTPResponse resp2 = http.GET("http://api.thingspeak.com/channels/1243676/feeds/last.txt")
				.addHeader(HOST, "api.thingspeak.com")
				.addHeader(CONNECTION, "close")
				.execute();
				
			sendStringlnUsart0(resp2.body);
				
				
		deleteHttp(&http);
	}
	
	deleteWifi(&wifi);

	while (true) {
	}
		
	
}

