#pragma once

#include "ESP8266_WiFi.h"
#include "HTTPClient.h"

HTTP *getWifiHttpInstance(WiFi *wifi);