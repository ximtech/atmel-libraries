#include "HTTPWifi.h"

static char *extractBody(char *rawResponse);
static HTTPStatus getHttpStatus(char *responseBody);

static WiFi *wifiPtr;

HTTP *getWifiHttpInstance(WiFi *wifi) {
	if (wifi != NULL) {
		wifiPtr = wifi;
		return getHttpInstance();
	}
	return NULL;
}

void sendRequestCallback(URL *url, HTTPResponse *httpResponse, char *requestBuffer) {
	char bufferPort[PORT_LENGTH] = { [0 ... PORT_LENGTH - 1] = 0 };
	itoa(url->port, bufferPort, 10);
	
	ResponseData *responseWifi = wifiPtr->connect(url->host, bufferPort);
	if (isResponseStatusSuccess(responseWifi)) {
		responseWifi = wifiPtr->send(requestBuffer);
		if (isResponseStatusSuccess(responseWifi) || isResponseStatusBufferFull(responseWifi)) {
			char *rawResponse = strstr_P(responseWifi->responseBody, PSTR(HTTP_VERSION_HEADER));
			httpResponse->rawResponse = rawResponse;
			httpResponse->body = extractBody(rawResponse);
			httpResponse->statusCode = getHttpStatus(responseWifi->responseBody);
		}
	}
}

static char *extractBody(char *rawResponse) {
	char *result = strrchr(rawResponse, '\n');
	if (result != NULL) {
		while(*(--result) != '\n');
		result++;
		
		char *responseStart = strstr_P(result, PSTR("+IPD,"));
		if (responseStart != NULL) {
			while (*(responseStart++) != ':');
			result = responseStart;
		}

		char *responseEnd = strstr_P(result, PSTR("CLOSED"));
		if (responseEnd != NULL) {
			*responseEnd = '\0';
		}
	}
	return result;
}

static HTTPStatus getHttpStatus(char *responseBody) {
	char buffRes[HTTP_STATUS_LENGTH] = { [0 ... HTTP_STATUS_LENGTH - 1] = 0 };
	size_t length = strlen_P(PSTR(HTTP_VERSION_HEADER)) + 2;	// for whitespace and line end
	char substringStart[length];
	memset(substringStart, 0, length);
	strcat(substringStart, HTTP_VERSION_HEADER);
	strcat(substringStart, " ");
	
	if (substring(substringStart, " ", responseBody, buffRes)) {
		return atoi(buffRes);
	}
	return 0;
}