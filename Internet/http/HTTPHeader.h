#pragma once

#include <string.h>
#include <avr/pgmspace.h>

typedef enum HTTPHeaderKey {
    EMPTY_HEADER_KEY,
    ACCEPT,
    ACCEPT_CHARSET,
    ACCEPT_ENCODING,
    ACCEPT_LANGUAGE,
    ACCEPT_RANGES,
    AGE,
    ALLOW,
    AUTHORIZATION,
    CACHE_CONTROL,
    CONNECTION,
    CONTENT_ENCODING,
    CONTENT_DISPOSITION,
    CONTENT_LANGUAGE,
    CONTENT_LENGTH,
    CONTENT_LOCATION,
    CONTENT_RANGE,
    CONTENT_TYPE,
    COOKIE,
    DATE,
    ETAG,
    EXPECT,
    EXPIRES,
    FROM,
    HOST,
    IF_MATCH,
    IF_MODIFIED_SINCE,
    IF_NONE_MATCH,
    IF_RANGE,
    IF_UNMODIFIED_SINCE,
    LAST_MODIFIED,
    LINK,
    LOCATION,
    MAX_FORWARDS,
    ORIGIN,
    PRAGMA,
    PROXY_AUTHENTICATE,
    PROXY_AUTHORIZATION,
    RANGE,
    REFERER,
    RETRY_AFTER,
    SERVER,
    SET_COOKIE,
    SET_COOKIE2,
    TE,
    TRAILER,
    TRANSFER_ENCODING,
    UPGRADE,
    USER_AGENT,
    VARY,
    VIA,
    WARNING,
    WWW_AUTHENTICATE,
} HTTPHeaderKey;


static const char PROGMEM EMPTY_HEADER_KEY_VALUE[] = "";
static const char PROGMEM ACCEPT_VALUE[] = "Accept";
static const char PROGMEM ACCEPT_CHARSET_VALUE[] = "Accept-Charset";
static const char PROGMEM ACCEPT_ENCODING_VALUE[] = "Accept-Encoding";
static const char PROGMEM ACCEPT_LANGUAGE_VALUE[] = "Accept-Language";
static const char PROGMEM ACCEPT_RANGES_VALUE[] = "Accept-Ranges";
static const char PROGMEM AGE_VALUE[] = "Age";
static const char PROGMEM ALLOW_VALUE[] = "Allow";
static const char PROGMEM CACHE_CONTROL_VALUE[] = "Cache-Control";
static const char PROGMEM AUTHORIZATION_VALUE[] = "Authorization";
static const char PROGMEM CONNECTION_VALUE[] = "Connection";
static const char PROGMEM CONTENT_ENCODING_VALUE[] = "Content-Encoding";
static const char PROGMEM CONTENT_DISPOSITION_VALUE[] = "Content-Disposition";
static const char PROGMEM CONTENT_LANGUAGE_VALUE[] = "Content-Language";
static const char PROGMEM CONTENT_LENGTH_VALUE[] = "Content-Length";
static const char PROGMEM CONTENT_LOCATION_VALUE[] = "Content-Location";
static const char PROGMEM CONTENT_RANGE_VALUE[] = "Content-Range";
static const char PROGMEM CONTENT_TYPE_VALUE[] = "Content-Type";
static const char PROGMEM COOKIE_VALUE[] = "Cookie";
static const char PROGMEM DATE_VALUE[] = "Date";
static const char PROGMEM ETAG_VALUE[] = "ETag";
static const char PROGMEM EXPECT_VALUE[] = "Expect";
static const char PROGMEM EXPIRES_VALUE[] = "Expires";
static const char PROGMEM FROM_VALUE[] = "From";
static const char PROGMEM HOST_VALUE[] = "Host";
static const char PROGMEM IF_MATCH_VALUE[] = "If-Match";
static const char PROGMEM IF_MODIFIED_SINCE_VALUE[] = "If-Modified-Since";
static const char PROGMEM IF_NONE_MATCH_VALUE[] = "If-None-Match";
static const char PROGMEM IF_RANGE_VALUE[] = "If-Range";
static const char PROGMEM IF_UNMODIFIED_SINCE_VALUE[] = "If-Unmodified-Since";
static const char PROGMEM LAST_MODIFIED_VALUE[] = "Last-Modified";
static const char PROGMEM LINK_VALUE[] = "Link";
static const char PROGMEM LOCATION_VALUE[] = "Location";
static const char PROGMEM MAX_FORWARDS_VALUE[] = "Max-Forwards";
static const char PROGMEM ORIGIN_VALUE[] = "Origin";
static const char PROGMEM PRAGMA_VALUE[] = "Pragma";
static const char PROGMEM PROXY_AUTHENTICATE_VALUE[] = "Proxy-Authenticate";
static const char PROGMEM PROXY_AUTHORIZATION_VALUE[] = "Proxy-Authorization";
static const char PROGMEM RANGE_VALUE[] = "Range";
static const char PROGMEM REFERER_VALUE[] = "Referer";
static const char PROGMEM RETRY_AFTER_VALUE[] = "Retry-After";
static const char PROGMEM SERVER_VALUE[] = "Server";
static const char PROGMEM SET_COOKIE_VALUE[] = "Set-Cookie";
static const char PROGMEM SET_COOKIE2_VALUE[] = "Set-Cookie2";
static const char PROGMEM TE_VALUE[] = "TE";
static const char PROGMEM TRAILER_VALUE[] = "Trailer";
static const char PROGMEM TRANSFER_ENCODING_VALUE[] = "Transfer-Encoding";
static const char PROGMEM UPGRADE_VALUE[] = "Upgrade";
static const char PROGMEM USER_AGENT_VALUE[] = "User-Agent";
static const char PROGMEM VARY_VALUE[] = "Vary";
static const char PROGMEM VIA_VALUE[] = "Via";
static const char PROGMEM WARNING_VALUE[] = "Warning";
static const char PROGMEM WWW_AUTHENTICATE_VALUE[] = "WWW-Authenticate";


static const char * const HEADER_KEY_LIST[] PROGMEM = {
        EMPTY_HEADER_KEY_VALUE,
        ACCEPT_VALUE,
        ACCEPT_CHARSET_VALUE,
        ACCEPT_ENCODING_VALUE,
        ACCEPT_LANGUAGE_VALUE,
        ACCEPT_RANGES_VALUE,
        AGE_VALUE,
        ALLOW_VALUE,
        CACHE_CONTROL_VALUE,
        AUTHORIZATION_VALUE,
        CONNECTION_VALUE,
        CONTENT_ENCODING_VALUE,
        CONTENT_DISPOSITION_VALUE,
        CONTENT_LANGUAGE_VALUE,
        CONTENT_LENGTH_VALUE,
        CONTENT_LOCATION_VALUE,
        CONTENT_RANGE_VALUE,
        CONTENT_TYPE_VALUE,
        COOKIE_VALUE,
        DATE_VALUE,
        ETAG_VALUE,
        EXPECT_VALUE,
        EXPIRES_VALUE,
        FROM_VALUE,
        HOST_VALUE,
        IF_MATCH_VALUE,
        IF_MODIFIED_SINCE_VALUE,
        IF_NONE_MATCH_VALUE,
        IF_RANGE_VALUE,
        IF_UNMODIFIED_SINCE_VALUE,
        LAST_MODIFIED_VALUE,
        LINK_VALUE,
        LOCATION_VALUE,
        MAX_FORWARDS_VALUE,
        ORIGIN_VALUE,
        PRAGMA_VALUE,
        PROXY_AUTHENTICATE_VALUE,
        PROXY_AUTHORIZATION_VALUE,
        RANGE_VALUE,
        REFERER_VALUE,
        RETRY_AFTER_VALUE,
        SERVER_VALUE,
        SET_COOKIE_VALUE,
        SET_COOKIE2_VALUE,
        TE_VALUE,
        TRAILER_VALUE,
        TRANSFER_ENCODING_VALUE,
        UPGRADE_VALUE,
        USER_AGENT_VALUE,
        VARY_VALUE,
        VIA_VALUE,
        WARNING_VALUE,
        WWW_AUTHENTICATE_VALUE
};

static char buffer[30];

static inline char * getHeaderValueByKey(HTTPHeaderKey headerKey) {
	strcpy_P(buffer, (char *)pgm_read_word( &HEADER_KEY_LIST[headerKey] ));
	return buffer;
}