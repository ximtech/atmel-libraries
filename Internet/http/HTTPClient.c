#include "HTTPClient.h"

#define HEADER_SEMICOLON ": "

typedef struct Header {
	HTTPHeaderKey headerKey;
	const char *value;
	const char *key;
	bool isCustomHeader;
} Header;

typedef struct HTTPQueryParameter {
    const char *key;
    const char *value;
} HTTPQueryParameter;

GET get(char *url);
GET bindGetParam(const char *key, const char *value);
GET addGetHeader(HTTPHeaderKey key, const char *value);
GET addGetCustomHeader(const char *key, const char *value);
HTTPResponse executeGet();

POST post(char *rawUrl);
POST bindPostParam(char *key, char *value);
POST bindJson(char *jsonString);
POST addPostHeader(HTTPHeaderKey key, char *value);
POST addPostCustomHeader(const char *key, const char *value);
HTTPResponse executePost();

PUT put(char *rawUrl);
PUT addPutHeader(HTTPHeaderKey key, char *value);
PUT addPutCustomHeader(const char *key, const char *value);
HTTPResponse executePut();

DELETE aDelete(char *rawUrl);
DELETE addDeleteHeader(HTTPHeaderKey key, char *value);
DELETE addDeleteCustomHeader(const char *key, const char *value);
HTTPResponse executeDelete();

HEAD head(char *rawUrl);
HEAD addHeadHeader(HTTPHeaderKey key, char *value);
HEAD addHeadCustomHeader(const char *key, const char *value);
HTTPResponse executeHead();

static void resetInnerData();
static void bindParam(const char *key, const char *value);
static void addHeader(HTTPHeaderKey key, const char *value);
static void addCustomHeader(const char *key, const char *value);
static HTTPResponse doExecute(const char *method);

static void parseURL(char *url, struct URL *resultURL);
static bool isHostPortPathParsed(char *url, struct URL *resultURL);
static bool isHostPathParsed(char *url, struct URL *resultURL);
static bool isHostParsed(char *url, struct URL *resultURL);

static uint16_t getUrlEncodedParamsLength();
static uint16_t getBodyEncodedParamsLength();
static uint16_t countParameterLength();
static uint16_t getHeadersLength();
static void addHeadersToRequest(char *requestBuffer);
static void addUrlEncodedParamsToRequest(char *requestBuffer);
static void addBodyEncodedParamsToRequest(char *requestBuffer);

static URL url;
static Header headers[HEADER_COUNT];
static HTTPQueryParameter queryParameters[HTTP_QUERY_PARAM_COUNT];

static uint8_t headerCounter;
static uint8_t parameterCounter;

static GET *httpGet;
static POST *httpPost;
static PUT *httpPut;
static DELETE *httpDelete;
static HEAD *httpHead;


HTTP *getHttpInstance() {
    HTTP *http = malloc(sizeof(HTTP));

    if (http != NULL) {
        http->GET = get;
        http->POST = post;
        http->PUT = put;
        http->DELETE = aDelete;
        http->HEAD = head;
    }
    return http;
}

void deleteHttp(HTTP *http) {
    if (httpGet != NULL) {
        free(httpGet);
    }
    if (httpPost != NULL) {
        free(httpPost);
    }
    if (httpPut != NULL) {
        free(httpPut);
    }
    if (httpDelete != NULL) {
        free(httpDelete);
    }
    if (httpHead != NULL) {
        free(httpHead);
    }

    free(http);
}


GET get(char *rawUrl) {
    if (httpGet == NULL) {
        httpGet = malloc(sizeof(GET));
        httpGet->bindParam = bindGetParam;
        httpGet->addHeader = addGetHeader;
        httpGet->addCustomHeader = addGetCustomHeader;
        httpGet->execute = executeGet;
    }

    if (httpGet != NULL) {
        resetInnerData();
        parseURL(rawUrl, &url);
    }
    return *httpGet;
}

GET bindGetParam(const char *key, const char *value) {
    bindParam(key, value);
    return *httpGet;
}

GET addGetHeader(HTTPHeaderKey key, const char *value) {
    addHeader(key, value);
    return *httpGet;
}

GET addGetCustomHeader(const char *key, const char *value) {
    addCustomHeader(key, value);
    return *httpGet;
}

HTTPResponse executeGet() {
	HTTPResponse httpResponse;
    if (url.isSuccessfullyParsed) {
        uint16_t bufferLength = 0;
        bufferLength += strlen_P(PSTR("GET /"));
        bufferLength += strlen(url.path);
        bufferLength += getUrlEncodedParamsLength();
		bufferLength += strlen_P(PSTR(" "));
        bufferLength += strlen_P(PSTR(HTTP_VERSION_HEADER));
		bufferLength += strlen_P(PSTR(NEW_LINE));
        bufferLength += getHeadersLength();
        bufferLength++;     // place for line end

        char buffer[bufferLength];
        memset(buffer, 0, bufferLength);
        sprintf_P(buffer, PSTR("GET /%s"), url.path);
        addUrlEncodedParamsToRequest(buffer);
		strcat_P(buffer, PSTR(" "));
        strcat_P(buffer, PSTR(HTTP_VERSION_HEADER));
		strcat_P(buffer, PSTR(NEW_LINE));
        addHeadersToRequest(buffer);
		sendRequestCallback(&url, &httpResponse, buffer);
    }
    return httpResponse;
}


POST post(char *rawUrl) {
    if (httpPost == NULL) {
        httpPost = malloc(sizeof(POST));
        httpPost->bindParam = bindPostParam;
        httpPost->bindJson = bindJson;
        httpPost->addHeader = addPostHeader;
        httpPost->addCustomHeader = addPostCustomHeader;
        httpPost->execute = executePost;
    }

    if (httpPost != NULL) {
        resetInnerData();
        parseURL(rawUrl, &url);
    }
    return *httpPost;
}

POST bindPostParam(char *key, char *value) {
    bindParam(key, value);
    return *httpPost;
}

POST bindJson(char *jsonString) {
    bindParam("", jsonString);
    return *httpPost;
}

POST addPostHeader(HTTPHeaderKey key, char *value) {
    addHeader(key, value);
    return *httpPost;
}

POST addPostCustomHeader(const char *key, const char *value) {
    addCustomHeader(key, value);
    return *httpPost;
}

HTTPResponse executePost() {
	HTTPResponse httpResponse;
    if (url.isSuccessfullyParsed) {
        uint16_t bufferLength = 0;
        bufferLength += strlen_P(PSTR("POST /"));
        bufferLength += strlen(url.path);
        bufferLength += getBodyEncodedParamsLength();
		bufferLength += strlen_P(PSTR(" "));
        bufferLength += strlen_P(PSTR(HTTP_VERSION_HEADER));
        bufferLength += strlen_P(PSTR(NEW_LINE));
        bufferLength += getHeadersLength();
        bufferLength++;     // place for line end

        char buffer[bufferLength];
        memset(buffer, 0, bufferLength);
        sprintf_P(buffer, PSTR("POST /%s"), url.path);
		strcat_P(buffer, PSTR(" "));
        strcat_P(buffer, PSTR(HTTP_VERSION_HEADER));
		strcat_P(buffer, PSTR(NEW_LINE));
        addHeadersToRequest(buffer);
        addBodyEncodedParamsToRequest(buffer);
		sendRequestCallback(&url, &httpResponse, buffer);
    }
    return httpResponse;
}

PUT put(char *rawUrl) {
    if (httpPut == NULL) {
        httpPut = malloc(sizeof(PUT));
        httpPut->addHeader = addPutHeader;
        httpPut->addCustomHeader = addPutCustomHeader;
        httpPut->execute = executePut;
    }

    if (httpPut != NULL) {
        resetInnerData();
        parseURL(rawUrl, &url);
    }
    return *httpPut;
}

PUT addPutHeader(HTTPHeaderKey key, char *value) {
    addHeader(key, value);
    return *httpPut;
}

PUT addPutCustomHeader(const char *key, const char *value) {
    addCustomHeader(key, value);
    return *httpPut;
}

HTTPResponse executePut() {
    return doExecute(PSTR("PUT"));
}

DELETE aDelete(char *rawUrl) {
    if (httpDelete == NULL) {
        httpDelete = malloc(sizeof(DELETE));
        httpDelete->addHeader = addDeleteHeader;
        httpDelete->addCustomHeader = addDeleteCustomHeader;
        httpDelete->execute = executeDelete;
    }

    if (httpDelete != NULL) {
        resetInnerData();
        parseURL(rawUrl, &url);
    }
    return *httpDelete;
}

DELETE addDeleteHeader(HTTPHeaderKey key, char *value) {
    addHeader(key, value);
    return *httpDelete;
}

DELETE addDeleteCustomHeader(const char *key, const char *value) {
    addCustomHeader(key, value);
    return *httpDelete;
}

HTTPResponse executeDelete() {
    return doExecute(PSTR("DELETE"));
}

HEAD head(char *rawUrl) {
    if (httpHead == NULL) {
        httpHead = malloc(sizeof(HEAD));
        httpHead->addHeader = addHeadHeader;
        httpHead->addCustomHeader = addHeadCustomHeader;
        httpHead->execute = executeHead;
    }

    if (httpHead != NULL) {
        resetInnerData();
        parseURL(rawUrl, &url);
    }
    return *httpHead;
}

HEAD addHeadHeader(HTTPHeaderKey key, char *value) {
    addHeader(key, value);
    return *httpHead;
}

HEAD addHeadCustomHeader(const char *key, const char *value) {
    addCustomHeader(key, value);
    return *httpHead;
}

HTTPResponse executeHead() {
    return doExecute(PSTR("HEAD"));
}

static void resetInnerData() {
    headerCounter = 0;
    parameterCounter = 0;
}

static void bindParam(const char *key, const char *value) {
    if (parameterCounter < HTTP_QUERY_PARAM_COUNT) {
        queryParameters[parameterCounter].key = key;
        queryParameters[parameterCounter].value = value;
        parameterCounter++;
    }
}

static void addHeader(HTTPHeaderKey key, const char *value) {
	if (headerCounter < HEADER_COUNT) {
		headers[headerCounter].headerKey = key;
		headers[headerCounter].value = value;
		headers[headerCounter].isCustomHeader = false;
		headerCounter++;
	}
}

static void addCustomHeader(const char *key, const char *value) {
    if (headerCounter < HEADER_COUNT) {
        headers[headerCounter].key = key;
        headers[headerCounter].value = value;
		headers[headerCounter].isCustomHeader = true;
        headerCounter++;
    }
}

HTTPResponse doExecute(const char *method) {
	HTTPResponse httpResponse;
    if (url.isSuccessfullyParsed) {
        uint16_t bufferLength = 0;
        bufferLength += strlen_P(method);
        bufferLength += strlen_P(PSTR(" /"));
        bufferLength += strlen(url.path);
		bufferLength += strlen_P(PSTR(" "));
        bufferLength += strlen_P(PSTR(HTTP_VERSION_HEADER));
        bufferLength += strlen_P(PSTR(NEW_LINE));
        bufferLength += getHeadersLength();
        bufferLength++;     // place for line end

        char buffer[bufferLength];
        memset(buffer, 0, bufferLength);
        strcat(buffer, method);
        strcat_P(buffer, PSTR(" /"));
        strcat(buffer, url.path);
		strcat_P(buffer, PSTR(" "));
        strcat(buffer, HTTP_VERSION_HEADER);
		strcat(buffer, NEW_LINE);
        addHeadersToRequest(buffer);
		sendRequestCallback(&url, &httpResponse, buffer);
    }
    return httpResponse;
}

static void parseURL(char *rawUrl, struct URL *resultURL) {
	if(strlen(rawUrl) <= (URL_HOST_LENGTH + URL_PATH_LENGTH + 1)) {
		memset(resultURL->host, 0, URL_HOST_LENGTH);
		memset(resultURL->path, 0, URL_PATH_LENGTH);
		resultURL->port = 80;
		resultURL->isSuccessfullyParsed =
			isHostPortPathParsed(rawUrl, resultURL) ||
			isHostPathParsed(rawUrl, resultURL) ||
			isHostParsed(rawUrl, resultURL);
	}
}

static bool isHostPortPathParsed(char *rawUrl, struct URL *resultURL) {
    return sscanf_P(rawUrl, PSTR("http://%99[^:]:%ui/%199[^\n]"), resultURL->host, &resultURL->port, resultURL->path) == 3;
}

static bool isHostPathParsed(char *rawUrl, struct URL *resultURL) {
    return sscanf_P(rawUrl, PSTR("http://%99[^/]/%199[^\n]"), resultURL->host, resultURL->path) == 2;
}

static bool isHostParsed(char *rawUrl, struct URL *resultURL) {
    return sscanf_P(rawUrl, PSTR("http://%99[^\n]"), resultURL->host) == 1;
}

static uint16_t getUrlEncodedParamsLength() {
    uint16_t paramLength = 0;
    paramLength += (parameterCounter * 4);// count of all "?", "="(2x) and "&" symbols. Total: 4 chars per each parameter
    paramLength += countParameterLength();
    return paramLength;
}

static uint16_t getBodyEncodedParamsLength() {
    uint16_t paramLength = 0;
    if (parameterCounter > 0) {
        paramLength += ((parameterCounter * 2) - 1);// count of all "=" and "&" symbols. (-1) -> no ampersand at first parameter
        paramLength += countParameterLength();
    }
    return paramLength;
}

static uint16_t countParameterLength() {
    uint16_t paramLength = 0;
    for (uint8_t i = 0; i < parameterCounter; ++i) {
        paramLength += strlen(queryParameters[i].key);
        paramLength += strlen(queryParameters[i].value);
    }
    return paramLength;
}

static uint16_t getHeadersLength() {
    uint16_t headerLength = 0;
    for (uint8_t i = 0; i < headerCounter; ++i) {
		
		Header header = headers[i];
		if (header.isCustomHeader) {
			if (strlen(header.key) > 0) {
				headerLength += strlen(headers[i].key);
				headerLength += strlen_P(HEADER_SEMICOLON);
			}
		} else {
			if (headers[i].headerKey != EMPTY_HEADER_KEY) {
				headerLength += strlen(getHeaderValueByKey(headers[i].headerKey));
				headerLength += strlen_P(HEADER_SEMICOLON);
			}
		}
		
        headerLength += strlen(headers[i].value);
		headerLength += strlen_P(PSTR(NEW_LINE));
    }

    if (headerCounter > 0) {
        headerLength += strlen_P(PSTR(NEW_LINE));
    }
    return headerLength;
}

static void addUrlEncodedParamsToRequest(char *requestBuffer) {
    for (uint8_t i = 0; i < parameterCounter; ++i) {
        if (i == 0) {
            strcat_P(requestBuffer, PSTR("?"));
        } else {
            strcat_P(requestBuffer, PSTR("&"));
        }
        strcat(requestBuffer, queryParameters[i].key);
        strcat_P(requestBuffer, PSTR("="));
        strcat(requestBuffer, queryParameters[i].value);
    }
}

static void addBodyEncodedParamsToRequest(char *requestBuffer) {
    for (uint8_t i = 0; i < parameterCounter; ++i) {
        if (i > 0) {
            strcat_P(requestBuffer, PSTR("&"));
        }

        if (strlen(queryParameters[i].key) > 0) { // check for adding not url like encoded parameters(json or plain data)
            strcat(requestBuffer, queryParameters[i].key);
            strcat_P(requestBuffer, PSTR("="));
        }
        strcat(requestBuffer, queryParameters[i].value);
    }

    if (parameterCounter > 0) {
        strcat_P(requestBuffer, PSTR(NEW_LINE));
        strcat_P(requestBuffer, PSTR(NEW_LINE));
    }
}

static void addHeadersToRequest(char *requestBuffer) {
    for (uint8_t i = 0; i < headerCounter; ++i) {
		
		Header header = headers[i];
		if (header.isCustomHeader) {
			if (strlen(header.key) > 0) {
				strcat(requestBuffer, header.key);
				strcat(requestBuffer, HEADER_SEMICOLON);
			}
		} else {
			if (headers[i].headerKey != EMPTY_HEADER_KEY) {
				strcat(requestBuffer, getHeaderValueByKey(headers[i].headerKey));
				strcat(requestBuffer, HEADER_SEMICOLON);
			}
		}
		
		strcat(requestBuffer, headers[i].value);
		strcat_P(requestBuffer, PSTR(NEW_LINE));
    }

    if (headerCounter > 0) {
        strcat_P(requestBuffer, PSTR(NEW_LINE));
    }
}