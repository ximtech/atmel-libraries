#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "HTTPHeader.h"
#include "HTTPStatus.h"

#define HTTP_VERSION_HEADER  "HTTP/1.1"
#define NEW_LINE			 "\r\n"
#define PORT_LENGTH		 	 5

#define URL_HOST_LENGTH  50
#define URL_PATH_LENGTH  50

#define HEADER_COUNT 		   5
#define HTTP_QUERY_PARAM_COUNT 5

#if URL_HOST_LENGTH > 100
#error "Host length can't be more than 100 characters"
#endif

#if URL_PATH_LENGTH > 200
#error "Url path length can't be more than 200 characters"
#endif

typedef struct URL {
	char host[URL_HOST_LENGTH];
	char path[URL_PATH_LENGTH];
	uint16_t port;
	bool isSuccessfullyParsed;
} URL;

typedef struct HTTPResponse {
    HTTPStatus statusCode;
	char *rawResponse;
    char *body;
} HTTPResponse;

typedef struct GET {
    struct GET (*bindParam)(const char *key, const char *value);
    struct GET (*addHeader)(HTTPHeaderKey key, const char *value);
    struct GET (*addCustomHeader)(const char *key, const char *value);
    HTTPResponse (*execute)();
} GET;

typedef struct POST {
    struct POST (*bindParam)(char *key, char *value);
    struct POST (*bindJson)(char *jsonString);
    struct POST (*addHeader)(HTTPHeaderKey key, char *value);
    struct POST (*addCustomHeader)(const char *key, const char *value);
    HTTPResponse (*execute)();
} POST;

typedef struct PUT {
    struct PUT (*addHeader)(HTTPHeaderKey key, char *value);
    struct PUT (*addCustomHeader)(const char *key, const char *value);
    HTTPResponse (*execute)();
} PUT;

typedef struct DELETE {
    struct DELETE (*addHeader)(HTTPHeaderKey key, char *value);
    struct DELETE (*addCustomHeader)(const char *key, const char *value);
    HTTPResponse (*execute)();
} DELETE;

typedef struct HEAD {
    struct HEAD (*addHeader)(HTTPHeaderKey key, char *value);
    struct HEAD (*addCustomHeader)(const char *key, const char *value);
    HTTPResponse (*execute)();
} HEAD;

typedef struct HTTP {
    struct GET (*GET)(char *host);
    struct POST (*POST)(char *host);
    struct PUT (*PUT)(char *host);
    struct DELETE (*DELETE)(char *host);
    struct HEAD (*HEAD)(char *host);
} HTTP;

extern void sendRequestCallback(URL *url, HTTPResponse *httpResponse, char *requestString);	// abstract function, must be implemented at the module inherited side

HTTP *getHttpInstance();
void deleteHttp(HTTP *http);