#pragma once

//#define F_CPU 8000000UL

#include <util/delay.h>
#include <stdint.h>

inline void DELAY_us(uint16_t us_count) {
	while (us_count != 0) {
		_delay_us(1);
		us_count--;
	}
}

inline void DELAY_ms(uint16_t ms_count) {
	while (ms_count != 0) {
		_delay_us(1000); //DELAY_us is called to generate 1ms delay
		ms_count--;
	}
}

inline void DELAY_sec(uint16_t sec_count) {
	while (sec_count != 0) {
		DELAY_ms(1000); //DELAY_ms is called to generate 1sec delay
		sec_count--;
	}
}