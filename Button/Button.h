#pragma once

/*
* button control library: lightweight library for controlling up to four buttons.
* Instruction:
* 1. Set F_CPU and if 16 bit timer is not use, then non blocked push button timer can be used (uncomment ENABLE_BUTTON_CLICK_CONTROL) and provide millis library
* 2. Set or use default button port and pin
* 3. Then initButton(), provide button count from enum, then put button ports as vararg parameter
* 4. To control button put listenButton(pin) routine to your code and listen button events with provided methods
* 5. To attach button interrupt, use attachButtonInterrupt() -> whith button pin and interrupt type(seen enums)
*/

//#define F_CPU 8000000UL
//#define ENABLE_BUTTON_CLICK_CONTROL	// additional button functions, use when timer 1 is not used

#include "Atmel_IO.h"

#ifdef ENABLE_BUTTON_CLICK_CONTROL	// import millis library 
#include "Millis/Millis.h"
#endif

#ifndef F_CPU
#error "F_CPU not defined in Button.h"
#endif

#define BUTTON_DEBOUNCE_MS 20UL
#define BUTTON_HOLD_TIMEOUT_MS 500UL
#define BUTTON_CLICK_TIMEOUT_MS 1000UL

typedef enum InterruptPin {
	BUTTON_INT0, BUTTON_INT1	
} InterruptPin;

typedef enum InterruptMode {
	LOW_LEVEL,
	LOGICAL_CHANGE,
	FAILING_EDGE,
	RISING_EDGE
} InterruptMode;

typedef struct Button {
	uint8_t pin;
	uint8_t clickCount;
	bool isPressed;
	bool isHolded;
#ifdef ENABLE_BUTTON_CLICK_CONTROL
	uint64_t clickTimer;
#endif
} Button;

void initButton(volatile Button *button, uint8_t pin) ;

bool isButtonPressed(volatile Button *button);
bool isButtonHolded(volatile Button *button);

#ifdef ENABLE_BUTTON_CLICK_CONTROL
_Bool isButtonSingleClicked(uint8_t pin);
_Bool isButtonDoubleClicked(uint8_t pin);
_Bool isButtonTripleClicked(uint8_t pin);
#endif

void listenButton(volatile Button *button);

void attachButtonInterrupt(InterruptPin interruptPin, InterruptMode interruptMode);
void removeInterrupt(InterruptPin interruptPin);