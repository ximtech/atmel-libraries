#include "Button.h"

#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))


void initButton(volatile Button *button, uint8_t pin) {
	if (button == NULL) return;
#ifdef ENABLE_BUTTON_CLICK_CONTROL
	initMillis();
#endif

    pinMode(pin, INPUT);
    digitalWrite(pin, HIGH);
	button->pin = pin;
	button->clickCount = 0;
	#ifdef ENABLE_BUTTON_CLICK_CONTROL
	button->clickTimer = BUTTON_CLICK_TIMEOUT_MS;
	#endif
	button->isPressed = false;
	button->isHolded = false;
}

bool isButtonPressed(volatile Button *button) {
	if (button != NULL && button->isPressed) {
		button->isPressed = false;
		button->clickCount = 0;
		return true;
	}
	return false;
}

bool isButtonHolded(volatile Button *button) {
	if (button != NULL && button->isHolded) {
		button->isPressed = false;
		button->isHolded = false;
		return true;
	}
	return false;
}

#ifdef ENABLE_BUTTON_CLICK_CONTROL
_Bool isButtonSingleClicked(uint8_t pin) {
	ButtonStatusHolder *buttonData = getButtonData(pin);
	if (buttonData != NULL && buttonData->clickCount == 1 && !buttonData->isHolded) {
		buttonData -> isPressed = false;
		return true;
	}
	return false;
}

_Bool isButtonDoubleClicked(uint8_t pin) {
	ButtonStatusHolder *buttonData = getButtonData(pin);
	if (buttonData != NULL && buttonData->clickCount == 2) {
		buttonData->isPressed = false;
		return true;
	}
	return false;
}

_Bool isButtonTripleClicked(uint8_t pin) {
	ButtonStatusHolder *buttonData = getButtonData(pin);
	if (buttonData != NULL && buttonData->clickCount == 3) {
		buttonData->isPressed = false;
		buttonData->clickCount = 0;
		return true;
	}
	return false;
}
#endif	// ENABLE_BUTTON_CLICK_CONTROL

void attachButtonInterrupt(InterruptPin interruptPin, InterruptMode interruptMode) {
	if (interruptPin == BUTTON_INT0) {
		GIMSK |= (1 << INT0);	
		switch(interruptMode) {
			case LOW_LEVEL:
				MCUCR &= ~(1 << ISC01);
				MCUCR &= ~(1 << ISC00);
				break;
			case LOGICAL_CHANGE:
				MCUCR &= ~(1 << ISC01);
				MCUCR |= (1 << ISC00);
				break;
			case FAILING_EDGE:
				MCUCR |= (1 << ISC01);
				MCUCR &= ~(1 << ISC00);
				break;
			case RISING_EDGE:
				MCUCR |= (1 << ISC01);
				MCUCR |= (1 << ISC00);
				break;
		}
		
	} else {
		GIMSK |= (1 << INT1);
		switch(interruptMode) {
			case LOW_LEVEL:
				MCUCR &= ~(1 << ISC11);
				MCUCR &= ~(1 << ISC10);
				break;
			case LOGICAL_CHANGE:
				MCUCR &= ~(1 << ISC11);
				MCUCR |= (1 << ISC10);
				break;
			case FAILING_EDGE:
				MCUCR |= (1 << ISC11);
				MCUCR &= ~(1 << ISC10);
				break;
			case RISING_EDGE:
				MCUCR |= (1 << ISC11);
				MCUCR |= (1 << ISC10);
				break;
		}
	}
	
	sei(); //  set interrupt enable bit
}

void removeInterrupt(InterruptPin interruptPin) {
	if (interruptPin == BUTTON_INT0) {
		GIMSK &= ~(1 << INT0);
	} else {
		GIMSK &= ~(1 << INT1);
	}
}

void listenButton(volatile Button *button) {
#ifndef ENABLE_BUTTON_CLICK_CONTROL	// slow button hold handling with blocking delay
		uint16_t timer = 0;
	
		while(digitalRead(button->pin) == LOW && timer <= BUTTON_HOLD_TIMEOUT_MS) {	// button hold down
			timer++; // count how long button is pressed
			_delay_ms(1);
		}
		
		if (timer >= BUTTON_DEBOUNCE_MS) {	// software debouncing button
			if (timer < BUTTON_HOLD_TIMEOUT_MS) {	// single click
				button->isHolded = false;
                button->clickCount++;
			} else {								// hold down
				button->isHolded = true;
                button->clickCount = 0;
			}
            button->isPressed = true;
	}
#endif

#ifdef ENABLE_BUTTON_CLICK_CONTROL	// fast non blocking handling
	uint32_t currentMilliseconds = millis();

	if (!button->isPressed && digitalRead(button->pin) == LOW) {	// button is pressed
		button->isPressed = true;
		button->clickCount++;
		button->clickTimer = currentMilliseconds;
	}
	
	if (button->isPressed) {
		if ((currentMilliseconds - button->clickTimer) >= BUTTON_HOLD_TIMEOUT_MS) {
			button->isHolded = true;
		} else {
			button->isHolded = false;
		}
	}

	if (button->clickCount > 0 && (currentMilliseconds - button->clickTimer >= BUTTON_CLICK_TIMEOUT_MS)) {	// click handling
		button->clickCount = 0;
	}
#endif
}